<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Standard">
<packages>
<package name="RESC1005X30" urn="urn:adsk.eagle:footprint:11214493/2">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-0.127" y1="0.254" x2="-0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="-0.254" x2="0.127" y2="0.254" width="0.0762" layer="21"/>
<wire x1="0.127" y1="0.254" x2="-0.127" y2="0.254" width="0.0762" layer="21"/>
</package>
<package name="CAPC1005X30" urn="urn:adsk.eagle:footprint:11214500/2">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5118" dy="0.6118" layer="1"/>
<text x="0" y="0.4929" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.4929" size="0.6096" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.0762" layer="21">
<vertex x="-0.127" y="0.254"/>
<vertex x="-0.127" y="-0.254"/>
<vertex x="0.127" y="-0.254"/>
<vertex x="0.127" y="0.254"/>
</polygon>
</package>
<package name="SOT95P280X145-5" urn="urn:adsk.eagle:footprint:11372015/2">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<circle x="-1.379" y="1.7941" radius="0.25" width="0" layer="21"/>
<wire x1="-0.875" y1="1.6041" x2="0.875" y2="1.6041" width="0.12" layer="21" style="shortdash"/>
<wire x1="-0.875" y1="-1.6041" x2="0.875" y2="-1.6041" width="0.12" layer="21"/>
<smd name="IN" x="-1.2533" y="0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="GND" x="-1.2533" y="0" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="EN" x="-1.2533" y="-0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="NC" x="1.2533" y="-0.95" dx="1.2088" dy="0.6802" layer="1"/>
<smd name="OUT" x="1.2533" y="0.95" dx="1.2088" dy="0.6802" layer="1"/>
<text x="0" y="2.6791" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2391" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
</package>
<package name="X4_THT" urn="urn:adsk.eagle:footprint:12077975/1">
<pad name="L" x="-7.62" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<pad name="ML" x="-2.54" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<pad name="MR" x="2.54" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<pad name="R" x="7.62" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<wire x1="0" y1="4.445" x2="-10.16" y2="4.445" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-3.4925" x2="-8.5725" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-8.5725" y1="-3.4925" x2="-6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="-3.4925" x2="-3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="-1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-3.4925" x2="0" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-10.16" y1="4.445" x2="-10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="-10.16" y1="2.54" x2="-9.8425" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.8425" y1="2.54" x2="-9.525" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-9.525" y1="2.2225" x2="-9.525" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-9.525" y1="1.5875" x2="-9.8425" y2="1.27" width="0.127" layer="21"/>
<wire x1="-9.8425" y1="1.27" x2="-10.16" y2="1.27" width="0.127" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-10.16" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-8.5725" y1="-3.4925" x2="-8.5725" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-8.5725" y1="-2.54" x2="-6.6675" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="-2.54" x2="-6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="-3.4925" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-2.54" x2="-1.5875" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-2.54" x2="-1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="10.16" y1="4.445" x2="0" y2="4.445" width="0.127" layer="21"/>
<wire x1="0" y1="-3.4925" x2="1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-3.4925" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-3.4925" x2="6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="6.6675" y1="-3.4925" x2="8.5725" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="8.5725" y1="-3.4925" x2="10.16" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="10.16" y1="-3.4925" x2="10.16" y2="1.27" width="0.127" layer="21"/>
<wire x1="10.16" y1="1.27" x2="10.4775" y2="1.27" width="0.127" layer="21"/>
<wire x1="10.4775" y1="1.27" x2="10.795" y2="1.5875" width="0.127" layer="21"/>
<wire x1="10.795" y1="1.5875" x2="10.795" y2="2.2225" width="0.127" layer="21"/>
<wire x1="10.795" y1="2.2225" x2="10.4775" y2="2.54" width="0.127" layer="21"/>
<wire x1="10.4775" y1="2.54" x2="10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="4.445" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-3.4925" x2="1.5875" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-2.54" x2="3.4925" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-2.54" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="6.6675" y1="-3.4925" x2="6.6675" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.6675" y1="-2.54" x2="8.5725" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.5725" y1="-2.54" x2="8.5725" y2="-3.4925" width="0.127" layer="21"/>
<rectangle x1="-10.16" y1="2.8575" x2="0" y2="4.445" layer="21"/>
<rectangle x1="0" y1="2.8575" x2="10.16" y2="4.445" layer="21"/>
<text x="-10.16" y="5.08" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X4_SMDCOM" urn="urn:adsk.eagle:footprint:12077974/1">
<smd name="2" x="0" y="-3.81" dx="3.81" dy="3.302" layer="1"/>
<smd name="1" x="0" y="3.81" dx="3.81" dy="3.302" layer="1"/>
<smd name="COM2" x="0" y="-0.9525" dx="3.81" dy="1.016" layer="1"/>
<smd name="COM1" x="0" y="0.9525" dx="3.81" dy="1.016" layer="1"/>
<text x="-1.27" y="5.715" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X3_SMDCOM" urn="urn:adsk.eagle:footprint:12077973/1">
<smd name="COM" x="0" y="0" dx="3.81" dy="1.016" layer="1"/>
<smd name="3" x="0" y="-2.54" dx="3.81" dy="3.302" layer="1"/>
<smd name="1" x="0" y="2.54" dx="3.81" dy="3.302" layer="1"/>
<text x="-1.27" y="5.08" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X2_SMD" urn="urn:adsk.eagle:footprint:12077977/1">
<smd name="2" x="0" y="-1.905" dx="3.81" dy="3.302" layer="1"/>
<smd name="1" x="0" y="1.905" dx="3.81" dy="3.302" layer="1"/>
<text x="-1.27" y="3.81" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X4_SMD" urn="urn:adsk.eagle:footprint:12077978/1">
<smd name="2" x="0" y="0" dx="3.81" dy="3.302" layer="1"/>
<smd name="3" x="0" y="-3.81" dx="3.81" dy="3.302" layer="1"/>
<smd name="1" x="0" y="3.81" dx="3.81" dy="3.302" layer="1"/>
<smd name="4" x="0" y="-7.62" dx="3.81" dy="3.302" layer="1"/>
<text x="-1.27" y="6.35" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X3_SMD" urn="urn:adsk.eagle:footprint:12077979/1">
<smd name="2" x="0" y="0" dx="3.81" dy="3.302" layer="1"/>
<smd name="3" x="0" y="-3.81" dx="3.81" dy="3.302" layer="1"/>
<smd name="1" x="0" y="3.81" dx="3.81" dy="3.302" layer="1"/>
<text x="-1.27" y="6.35" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="X2_THT" urn="urn:adsk.eagle:footprint:12077976/1">
<pad name="L" x="-2.54" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<pad name="R" x="2.54" y="0" drill="1.1" diameter="2.1844" shape="long" rot="R90"/>
<wire x1="5.08" y1="4.445" x2="-5.08" y2="4.445" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.4925" x2="-3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="-1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-3.4925" x2="1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-3.4925" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-3.4925" x2="5.08" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-4.7625" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="2.54" x2="-4.445" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-4.445" y1="2.2225" x2="-4.445" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-4.445" y1="1.5875" x2="-4.7625" y2="1.27" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="1.27" x2="-5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.4925" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.3975" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.3975" y1="1.27" x2="5.715" y2="1.5875" width="0.127" layer="21"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="2.2225" width="0.127" layer="21"/>
<wire x1="5.715" y1="2.2225" x2="5.3975" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.3975" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="4.445" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-3.4925" x2="-3.4925" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-2.54" x2="-1.5875" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-2.54" x2="-1.5875" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-3.4925" x2="1.5875" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-2.54" x2="3.4925" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-2.54" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<rectangle x1="-5.08" y1="2.8575" x2="5.08" y2="4.445" layer="21"/>
<text x="-5.08" y="5.08" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="UFQFPN28" urn="urn:adsk.eagle:footprint:12074858/1">
<smd name="PA5" x="0" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA4" x="-0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA3" x="-1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA6" x="0.5" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA7" x="1" y="-1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA2" x="-1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PB0" x="1.5" y="-1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PA1" x="-1.975" y="-1.5" dx="0.35" dy="0.3" layer="1"/>
<smd name="PA0" x="-1.875" y="-1" dx="0.55" dy="0.3" layer="1"/>
<smd name="VDDA" x="-1.875" y="-0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="!RST" x="-1.875" y="0" dx="0.55" dy="0.3" layer="1"/>
<smd name="OSC_OUT" x="-1.875" y="0.5" dx="0.55" dy="0.3" layer="1"/>
<smd name="OSC_IN" x="-1.875" y="1" dx="0.55" dy="0.3" layer="1"/>
<smd name="BOOT0" x="-1.975" y="1.5" dx="0.35" dy="0.3" layer="1"/>
<smd name="PB7" x="-1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PB6" x="-1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB5" x="-0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB4" x="0" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PB3" x="0.5" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA15" x="1" y="1.875" dx="0.55" dy="0.3" layer="1" rot="R90"/>
<smd name="PA14" x="1.5" y="1.975" dx="0.35" dy="0.3" layer="1" rot="R90"/>
<smd name="PA13" x="1.975" y="1.5" dx="0.35" dy="0.3" layer="1" rot="R180"/>
<smd name="PA10" x="1.875" y="1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="PA9" x="1.875" y="0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VDDIO2" x="1.875" y="0" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VDD" x="1.875" y="-0.5" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="VSS" x="1.875" y="-1" dx="0.55" dy="0.3" layer="1" rot="R180"/>
<smd name="PB1" x="1.975" y="-1.5" dx="0.35" dy="0.3" layer="1" rot="R180"/>
<wire x1="-2.05" y1="2.05" x2="-2.05" y2="1.8" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-1.8" y2="2.05" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-2.05" y2="-1.8" width="0.0762" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.8" y2="-2.05" width="0.0762" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="1.8" y2="-2.05" width="0.0762" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="2.05" y2="-1.8" width="0.0762" layer="21"/>
<wire x1="2.05" y1="2.05" x2="2.05" y2="1.8" width="0.0762" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.8" y2="2.05" width="0.0762" layer="21"/>
<circle x="-1.15" y="1.05" radius="0.2" width="0.0762" layer="21"/>
<text x="-1.4" y="-1.35" size="0.6096" layer="21" font="vector">&gt;NAME</text>
<polygon width="0" layer="31">
<vertex x="-1.8" y="1.65"/>
<vertex x="-1.65" y="1.5"/>
<vertex x="-1.65" y="1.35"/>
<vertex x="-1.8" y="1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.65" y="1.8"/>
<vertex x="-1.5" y="1.65"/>
<vertex x="-1.35" y="1.65"/>
<vertex x="-1.35" y="1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.65" y="1.8"/>
<vertex x="1.5" y="1.65"/>
<vertex x="1.35" y="1.65"/>
<vertex x="1.35" y="1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.8" y="1.65"/>
<vertex x="1.65" y="1.5"/>
<vertex x="1.65" y="1.35"/>
<vertex x="1.8" y="1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.65" y="-1.8"/>
<vertex x="-1.5" y="-1.65"/>
<vertex x="-1.35" y="-1.65"/>
<vertex x="-1.35" y="-1.8"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.8" y="-1.65"/>
<vertex x="-1.65" y="-1.5"/>
<vertex x="-1.65" y="-1.35"/>
<vertex x="-1.8" y="-1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.8" y="-1.65"/>
<vertex x="1.65" y="-1.5"/>
<vertex x="1.65" y="-1.35"/>
<vertex x="1.8" y="-1.35"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="1.65" y="-1.8"/>
<vertex x="1.5" y="-1.65"/>
<vertex x="1.35" y="-1.65"/>
<vertex x="1.35" y="-1.8"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.8381" y="1.7262"/>
<vertex x="-1.6246" y="1.5127"/>
<vertex x="-1.6246" y="1.3119"/>
<vertex x="-1.8381" y="1.3119"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.7389" y="1.8254"/>
<vertex x="-1.5254" y="1.6119"/>
<vertex x="-1.3246" y="1.6119"/>
<vertex x="-1.3246" y="1.8254"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.7262" y="-1.8127"/>
<vertex x="-1.5127" y="-1.5992"/>
<vertex x="-1.3119" y="-1.5992"/>
<vertex x="-1.3119" y="-1.8127"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-1.8254" y="-1.7135"/>
<vertex x="-1.6119" y="-1.5"/>
<vertex x="-1.6119" y="-1.2992"/>
<vertex x="-1.8254" y="-1.2992"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.8254" y="-1.7135"/>
<vertex x="1.6119" y="-1.5"/>
<vertex x="1.6119" y="-1.2992"/>
<vertex x="1.8254" y="-1.2992"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.7262" y="-1.8127"/>
<vertex x="1.5127" y="-1.5992"/>
<vertex x="1.3119" y="-1.5992"/>
<vertex x="1.3119" y="-1.8127"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.7262" y="1.8381"/>
<vertex x="1.5127" y="1.6246"/>
<vertex x="1.3119" y="1.6246"/>
<vertex x="1.3119" y="1.8381"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="1.8254" y="1.7389"/>
<vertex x="1.6119" y="1.5254"/>
<vertex x="1.6119" y="1.3246"/>
<vertex x="1.8254" y="1.3246"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-2.15" y="-1.35"/>
<vertex x="-1.65" y="-1.35"/>
<vertex x="-1.65" y="-1.5"/>
<vertex x="-1.8" y="-1.65"/>
<vertex x="-2.15" y="-1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.65" y="-1.8"/>
<vertex x="-1.5" y="-1.65"/>
<vertex x="-1.35" y="-1.65"/>
<vertex x="-1.35" y="-2.15"/>
<vertex x="-1.65" y="-2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.35" y="-2.15"/>
<vertex x="1.35" y="-1.65"/>
<vertex x="1.5" y="-1.65"/>
<vertex x="1.65" y="-1.8"/>
<vertex x="1.65" y="-2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.8" y="-1.65"/>
<vertex x="1.65" y="-1.5"/>
<vertex x="1.65" y="-1.35"/>
<vertex x="2.15" y="-1.35"/>
<vertex x="2.15" y="-1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.35" y="2.15"/>
<vertex x="-1.35" y="1.65"/>
<vertex x="-1.5" y="1.65"/>
<vertex x="-1.65" y="1.8"/>
<vertex x="-1.65" y="2.15"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="-1.8" y="1.65"/>
<vertex x="-1.65" y="1.5"/>
<vertex x="-1.65" y="1.35"/>
<vertex x="-2.15" y="1.35"/>
<vertex x="-2.15" y="1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="2.15" y="1.35"/>
<vertex x="1.65" y="1.35"/>
<vertex x="1.65" y="1.5"/>
<vertex x="1.8" y="1.65"/>
<vertex x="2.15" y="1.65"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="1.65" y="1.8"/>
<vertex x="1.5" y="1.65"/>
<vertex x="1.35" y="1.65"/>
<vertex x="1.35" y="2.15"/>
<vertex x="1.65" y="2.15"/>
</polygon>
</package>
<package name="SON65P300X300X90-9T170X250" urn="urn:adsk.eagle:footprint:11209461/2">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 0.90 mm body, 2.50 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 0.90 mm and thermal pad size 2.50 X 1.70 mm&lt;/p&gt;</description>
<circle x="-2.004" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-1.5" y1="1.389" x2="-1.5" y2="1.564" width="0.12" layer="21"/>
<wire x1="-1.5" y1="1.564" x2="1.5" y2="1.564" width="0.12" layer="21"/>
<wire x1="1.5" y1="1.564" x2="1.5" y2="1.389" width="0.12" layer="21"/>
<wire x1="-1.5" y1="-1.389" x2="-1.5" y2="-1.564" width="0.12" layer="21"/>
<wire x1="-1.5" y1="-1.564" x2="1.5" y2="-1.564" width="0.12" layer="21"/>
<wire x1="1.5" y1="-1.564" x2="1.5" y2="-1.389" width="0.12" layer="21"/>
<smd name="1" x="-1.4506" y="0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="2" x="-1.4506" y="0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="3" x="-1.4506" y="-0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="4" x="-1.4506" y="-0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="5" x="1.4506" y="-0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="6" x="1.4506" y="-0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="7" x="1.4506" y="0.325" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="8" x="1.4506" y="0.975" dx="0.8107" dy="0.32" layer="1" roundness="100"/>
<smd name="9" x="0" y="0" dx="1.7" dy="2.5" layer="1" thermals="no"/>
<text x="0" y="1.889" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
</package>
<package name="SWD" urn="urn:adsk.eagle:footprint:12074857/1">
<text x="-1.27" y="5.08" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="1.27" y="2.54" size="0.6096" layer="21" font="vector" align="center">+</text>
<text x="0.9525" y="0.9525" size="0.6096" layer="21" font="vector">SWC</text>
<text x="0.9525" y="-0.3175" size="0.6096" layer="21" font="vector">SWD</text>
<text x="1.27" y="-1.27" size="0.6096" layer="21" font="vector" align="center">-</text>
<text x="1.5875" y="-2.54" size="0.6096" layer="21" font="vector" align="center">!RST</text>
<pad name="VCC" x="0" y="2.54" drill="0.5"/>
<pad name="RST" x="0" y="-2.54" drill="0.5"/>
<pad name="SWC" x="0" y="1.27" drill="0.5"/>
<pad name="SWD" x="0" y="0" drill="0.5"/>
<pad name="VSS" x="0" y="-1.27" drill="0.5"/>
</package>
<package name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:footprint:11816613/2">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<circle x="-2.554" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-2.05" y1="1.389" x2="-2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="1.389" x2="2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-1.389" x2="2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="1.389" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-1.389" x2="-2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.389" y2="-2.05" width="0.12" layer="21"/>
<smd name="1" x="-1.9346" y="1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="2" x="-1.9346" y="0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="3" x="-1.9346" y="0" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="4" x="-1.9346" y="-0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="5" x="-1.9346" y="-1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="6" x="-1" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-0.5" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="0.5" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="1" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="1.9346" y="-1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="12" x="1.9346" y="-0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="13" x="1.9346" y="0" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="14" x="1.9346" y="0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="15" x="1.9346" y="1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="16" x="1" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.5" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="0" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="-0.5" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="-1" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="0" y="0" dx="2.75" dy="2.75" layer="1" thermals="no"/>
<text x="0" y="2.9996" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9996" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="ACS711" urn="urn:adsk.eagle:footprint:12077855/1">
<smd name="1-2" x="-1.35" y="0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="3-4" x="-1.35" y="-0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="9" x="1.35" y="0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="1.35" y="-0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="7" x="1.35" y="-0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="1.35" y="0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="6" x="0.65" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="0.15" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.65" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.15" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-0.1875" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.9375" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.125" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="0.9375" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-0.1875" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.125" width="0.127" layer="21"/>
<circle x="-2.0625" y="1.875" radius="0.1875" width="0.127" layer="21"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="L_140120" urn="urn:adsk.eagle:footprint:12077853/1">
<smd name="P$1" x="-6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<smd name="P$2" x="6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<wire x1="-7" y1="6" x2="-7" y2="3" width="0.1524" layer="21"/>
<wire x1="-7" y1="6" x2="7" y2="6" width="0.1524" layer="21"/>
<wire x1="7" y1="6" x2="7" y2="3" width="0.1524" layer="21"/>
<wire x1="7" y1="-3" x2="7" y2="-7" width="0.1524" layer="21"/>
<wire x1="7" y1="-7" x2="-7" y2="-7" width="0.1524" layer="21"/>
<wire x1="-7" y1="-7" x2="-7" y2="-3" width="0.1524" layer="21"/>
<text x="-5.715" y="4.445" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-6.35" y="-6.35" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L_100115" urn="urn:adsk.eagle:footprint:12077881/1">
<smd name="P$1" x="-4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<smd name="P$2" x="4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<wire x1="-6" y1="2.25" x2="-6" y2="5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="5.25" x2="6" y2="5.25" width="0.127" layer="21"/>
<wire x1="6" y1="5.25" x2="6" y2="2.25" width="0.127" layer="21"/>
<wire x1="6" y1="-2.25" x2="6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="6" y1="-5.25" x2="-6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="-5.25" x2="-6" y2="-2.25" width="0.127" layer="21"/>
<text x="-5.25" y="4.25" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-5.5" y="-4.75" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="RESC2012X70N" urn="urn:adsk.eagle:footprint:13124728/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<smd name="1" x="-0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-0.15875" y1="0.714375" x2="-0.15875" y2="-0.714375" width="0.127" layer="21"/>
<wire x1="-0.15875" y1="-0.714375" x2="0.15875" y2="-0.714375" width="0.127" layer="21"/>
<wire x1="0.15875" y1="-0.714375" x2="0.15875" y2="0.714375" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.714375" x2="-0.15875" y2="0.714375" width="0.127" layer="21"/>
</package>
<package name="CAPC2012X70N" urn="urn:adsk.eagle:footprint:13124732/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<smd name="1" x="-0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8754" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.238125" y1="-0.714375" x2="0.238125" y2="0.714375" layer="21"/>
</package>
<package name="CAPC3216X160" urn="urn:adsk.eagle:footprint:11819575/2">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<smd name="1" x="-1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<text x="0" y="1.1699" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1699" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="21"/>
</package>
<package name="X3S_THT" urn="urn:adsk.eagle:footprint:12077972/1" locally_modified="yes">
<pad name="2" x="0" y="0" drill="0.75"/>
<pad name="1" x="0" y="1.524" drill="0.75"/>
<pad name="3" x="0" y="-1.524" drill="0.75"/>
</package>
<package name="PG-TDSON-8" urn="urn:adsk.eagle:footprint:12077854/1">
<smd name="3" x="2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="1" x="2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="4" x="2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="6" x="-2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="7" x="-2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="8" x="-2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="5" x="-2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<polygon width="0.002540625" layer="1">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="2.285"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<circle x="3.81" y="-2.8575" radius="0.3175" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.54" x2="2.8575" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.8575" y1="-2.54" x2="-3.4925" y2="-2.54" width="0.127" layer="21"/>
<text x="-3.4925" y="2.8575" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-3.4925" y="-3.4925" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1005X30" urn="urn:adsk.eagle:package:11214492/2" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X30"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X30" urn="urn:adsk.eagle:package:11214499/2" type="model">
<description>Chip, 1.00 X 0.50 X 0.30 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.30 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X30"/>
</packageinstances>
</package3d>
<package3d name="SOT95P280X145-5" urn="urn:adsk.eagle:package:11372008/2" type="model">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X145-5"/>
</packageinstances>
</package3d>
<package3d name="X4_THT" urn="urn:adsk.eagle:package:12077990/3" type="model">
<packageinstances>
<packageinstance name="X4_THT"/>
</packageinstances>
</package3d>
<package3d name="X4_SMDCOM" urn="urn:adsk.eagle:package:12077989/1" type="box">
<packageinstances>
<packageinstance name="X4_SMDCOM"/>
</packageinstances>
</package3d>
<package3d name="X4_SMD" urn="urn:adsk.eagle:package:12077993/1" type="box">
<packageinstances>
<packageinstance name="X4_SMD"/>
</packageinstances>
</package3d>
<package3d name="X3_SMDCOM" urn="urn:adsk.eagle:package:12077988/1" type="box">
<packageinstances>
<packageinstance name="X3_SMDCOM"/>
</packageinstances>
</package3d>
<package3d name="X3_SMD" urn="urn:adsk.eagle:package:12077994/1" type="box">
<packageinstances>
<packageinstance name="X3_SMD"/>
</packageinstances>
</package3d>
<package3d name="X2_SMD" urn="urn:adsk.eagle:package:12077992/1" type="box">
<packageinstances>
<packageinstance name="X2_SMD"/>
</packageinstances>
</package3d>
<package3d name="X2_THT" urn="urn:adsk.eagle:package:12077991/1" type="box">
<packageinstances>
<packageinstance name="X2_THT"/>
</packageinstances>
</package3d>
<package3d name="UFQFPN28" urn="urn:adsk.eagle:package:12074869/2" type="model">
<packageinstances>
<packageinstance name="UFQFPN28"/>
</packageinstances>
</package3d>
<package3d name="SON65P300X300X90-9T170X250" urn="urn:adsk.eagle:package:11209438/2" type="model">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 0.90 mm body, 2.50 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 0.90 mm and thermal pad size 2.50 X 1.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SON65P300X300X90-9T170X250"/>
</packageinstances>
</package3d>
<package3d name="SWD" urn="urn:adsk.eagle:package:12074868/1" type="box">
<packageinstances>
<packageinstance name="SWD"/>
</packageinstances>
</package3d>
<package3d name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:package:11816611/2" type="model">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN50P400X400X80-21T275"/>
</packageinstances>
</package3d>
<package3d name="ACS711" urn="urn:adsk.eagle:package:12077887/3" type="model">
<packageinstances>
<packageinstance name="ACS711"/>
</packageinstances>
</package3d>
<package3d name="L_140120" urn="urn:adsk.eagle:package:12077885/2" type="model">
<packageinstances>
<packageinstance name="L_140120"/>
</packageinstances>
</package3d>
<package3d name="L_100115" urn="urn:adsk.eagle:package:12077884/2" type="model">
<packageinstances>
<packageinstance name="L_100115"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70N" urn="urn:adsk.eagle:package:13124700/1" type="model">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70N"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X70N" urn="urn:adsk.eagle:package:13124731/1" type="model">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X70N"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X160" urn="urn:adsk.eagle:package:11819311/2" type="model">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3216X160"/>
</packageinstances>
</package3d>
<package3d name="X3S_THT" urn="urn:adsk.eagle:package:12077987/1" type="box">
<packageinstances>
<packageinstance name="X3S_THT"/>
</packageinstances>
</package3d>
<package3d name="PG-TDSON-8" urn="urn:adsk.eagle:package:12077886/2" type="model">
<packageinstances>
<packageinstance name="PG-TDSON-8"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:12077983/1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<wire x1="203.835" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="203.835" y2="8.89" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="3.81" width="0.1016" layer="94"/>
<wire x1="203.835" y1="8.89" x2="203.835" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="203.835" y1="13.97" x2="203.835" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="203.835" y1="19.05" x2="203.835" y2="24.13" width="0.1016" layer="94"/>
<text x="205.74" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="205.74" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="219.075" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="205.486" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="205.74" y="20.32" size="2.54" layer="94" font="vector">Moritz Geier</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
<symbol name="R" urn="urn:adsk.eagle:symbol:12077986/1">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
</symbol>
<symbol name="C" urn="urn:adsk.eagle:symbol:12077985/1">
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="-1.905" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="1.905" y2="0.635" width="0.3048" layer="94"/>
<text x="2.54" y="-1.905" size="0.8128" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="95" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="UREG" urn="urn:adsk.eagle:symbol:12077984/1">
<pin name="IN" x="-7.62" y="0" visible="off" length="short"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="EN" x="-2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.81" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="0" size="0.8128" layer="97" font="vector">IN</text>
<text x="3.81" y="0" size="0.8128" layer="97" font="vector" align="bottom-right">OUT</text>
<text x="2.54" y="-1.27" size="0.8128" layer="97" font="vector" align="center">GND</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="97" font="vector" align="center">EN</text>
<text x="5.08" y="-5.08" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="X4" urn="urn:adsk.eagle:symbol:12077980/1">
<pin name="1" x="5.08" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="4" x="5.08" y="-7.62" visible="pad" length="short" rot="R180"/>
<wire x1="2.54" y1="8.89" x2="2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="-3.81" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-8.89" x2="-3.81" y2="8.89" width="0.254" layer="94"/>
<wire x1="-3.81" y1="8.89" x2="2.54" y2="8.89" width="0.254" layer="94"/>
<text x="-3.81" y="10.16" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="X3" urn="urn:adsk.eagle:symbol:12077982/1">
<pin name="1" x="5.08" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="3" x="5.08" y="-5.08" visible="pad" length="short" rot="R180"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-6.35" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="-3.81" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-3.81" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="X2" urn="urn:adsk.eagle:symbol:12077981/1">
<pin name="1" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-3.81" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<text x="-3.81" y="5.08" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="STM32F04_AF">
<pin name="!RST" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="OSC_OUT" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="BOOT0" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="OSC_IN" x="5.08" y="0" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-11.43" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="-6.35" x2="-11.43" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="3.81" x2="2.54" y2="3.81" width="0.1524" layer="94"/>
<text x="-11.43" y="5.08" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="STM32F04_PA">
<pin name="PA0" x="5.08" y="15.24" length="short" rot="R180"/>
<pin name="PA1" x="5.08" y="12.7" length="short" rot="R180"/>
<pin name="PA2" x="5.08" y="10.16" length="short" rot="R180"/>
<pin name="PA3" x="5.08" y="7.62" length="short" rot="R180"/>
<pin name="PA4" x="5.08" y="5.08" length="short" rot="R180"/>
<pin name="PA5" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="PA6" x="5.08" y="0" length="short" rot="R180"/>
<pin name="PA7" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="PA9" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="PA10" x="5.08" y="-7.62" length="short" rot="R180"/>
<pin name="PA13" x="5.08" y="-10.16" length="short" rot="R180"/>
<pin name="PA14" x="5.08" y="-12.7" length="short" rot="R180"/>
<pin name="PA15" x="5.08" y="-15.24" length="short" rot="R180"/>
<wire x1="2.54" y1="16.51" x2="2.54" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-16.51" x2="-6.35" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-16.51" x2="-6.35" y2="16.51" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="16.51" x2="2.54" y2="16.51" width="0.1524" layer="94"/>
<text x="-6.35" y="17.78" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="STM32F04_PB">
<wire x1="2.54" y1="8.89" x2="2.54" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="-6.35" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-8.89" x2="-6.35" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="8.89" x2="2.54" y2="8.89" width="0.1524" layer="94"/>
<text x="-6.35" y="10.16" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="PB0" x="5.08" y="7.62" length="short" rot="R180"/>
<pin name="PB1" x="5.08" y="5.08" length="short" rot="R180"/>
<pin name="PB3" x="5.08" y="2.54" length="short" rot="R180"/>
<pin name="PB4" x="5.08" y="0" length="short" rot="R180"/>
<pin name="PB5" x="5.08" y="-2.54" length="short" rot="R180"/>
<pin name="PB6" x="5.08" y="-5.08" length="short" rot="R180"/>
<pin name="PB7" x="5.08" y="-7.62" length="short" rot="R180"/>
</symbol>
<symbol name="STM32F04_PWR">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VDDIO2" x="5.08" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VDDA" x="-5.08" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="-5.08" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="5.08" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="4.445" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDDIO2</text>
<text x="-5.715" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDDA</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
</symbol>
<symbol name="ATA6561">
<pin name="STBY" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="TXD" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="RXD" x="-10.16" y="-5.08" length="short" direction="out"/>
<pin name="CANH" x="10.16" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="CANL" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-6.35" x2="7.62" y2="6.35" width="0.1524" layer="94"/>
<wire x1="7.62" y1="6.35" x2="-7.62" y2="6.35" width="0.1524" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="ATA6561_PWR">
<pin name="VDD" x="2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<pin name="VDD1" x="-2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<circle x="2.54" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="-2.54" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.064" width="0.1524" layer="94"/>
<text x="1.905" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-3.175" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VIO</text>
</symbol>
<symbol name="SWD">
<pin name="SWD" x="5.08" y="0" visible="pin" length="short" rot="R180"/>
<pin name="SWCLK" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="VCC" x="5.08" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="!RST" x="5.08" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="ADP1853">
<pin name="ENB" x="-12.7" y="15.24" visible="pin" length="short"/>
<pin name="TRK" x="-12.7" y="12.7" visible="pin" length="short"/>
<pin name="VCCO" x="-12.7" y="10.16" visible="pin" length="short"/>
<pin name="SYNC" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="FREQ" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="SS" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="RAMP" x="-12.7" y="17.78" visible="pin" length="short"/>
<pin name="DH" x="12.7" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="BST" x="12.7" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="SW" x="12.7" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="CS" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="ILIM" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DL" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="FB" x="12.7" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="COMP" x="12.7" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="PGOOD" x="12.7" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="CLKOUT" x="12.7" y="-20.32" visible="pin" length="short" rot="R180"/>
<wire x1="-10.16" y1="19.05" x2="-10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-21.59" x2="10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="10.16" y1="-21.59" x2="10.16" y2="19.05" width="0.254" layer="94"/>
<wire x1="10.16" y1="19.05" x2="-10.16" y2="19.05" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="PWR-3">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="ACS711">
<pin name="IP+" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="IP-" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="VIOUT" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="!FAULT" x="10.16" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="PWR-2">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="L">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.9525" x2="2.54" y2="0.9525" layer="94"/>
</symbol>
<symbol name="NMOS">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="6.35" y="-1.27" size="0.8128" layer="96" rot="MR270" align="bottom-right">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:12077999/1" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" urn="urn:adsk.eagle:component:12078002/1" locally_modified="yes" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214492/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13124700/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" urn="urn:adsk.eagle:component:12078001/1" locally_modified="yes" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X30">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11214499/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X70N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13124731/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X160">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11819311/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UREG" urn="urn:adsk.eagle:component:12078000/1" prefix="UREG" uservalue="yes">
<gates>
<gate name="G$1" symbol="UREG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5">
<connects>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11372008/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="X4" urn="urn:adsk.eagle:component:12077995/1" locally_modified="yes" prefix="X">
<gates>
<gate name="G$1" symbol="X4" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="X4_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077993/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THT" package="X4_THT">
<connects>
<connect gate="G$1" pin="1" pad="L"/>
<connect gate="G$1" pin="2" pad="ML"/>
<connect gate="G$1" pin="3" pad="MR"/>
<connect gate="G$1" pin="4" pad="R"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077990/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_COM" package="X4_SMDCOM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="COM1"/>
<connect gate="G$1" pin="3" pad="COM2"/>
<connect gate="G$1" pin="4" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077989/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="X3" urn="urn:adsk.eagle:component:12077997/1" prefix="X">
<gates>
<gate name="G$1" symbol="X3" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="X3_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077994/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDCOM" package="X3_SMDCOM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="COM"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077988/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THTSMALL" package="X3S_THT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077987/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="X2" urn="urn:adsk.eagle:component:12077998/1" prefix="X">
<gates>
<gate name="G$1" symbol="X2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="X2_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077992/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THT" package="X2_THT">
<connects>
<connect gate="G$1" pin="1" pad="L"/>
<connect gate="G$1" pin="2" pad="R"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077991/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F042G6" prefix="MCU">
<gates>
<gate name="PF" symbol="STM32F04_AF" x="-15.24" y="12.7"/>
<gate name="PA" symbol="STM32F04_PA" x="-20.32" y="-17.78"/>
<gate name="PB" symbol="STM32F04_PB" x="2.54" y="-10.16"/>
<gate name="PWR" symbol="STM32F04_PWR" x="27.94" y="10.16"/>
</gates>
<devices>
<device name="" package="UFQFPN28">
<connects>
<connect gate="PA" pin="PA0" pad="PA0"/>
<connect gate="PA" pin="PA1" pad="PA1"/>
<connect gate="PA" pin="PA10" pad="PA10"/>
<connect gate="PA" pin="PA13" pad="PA13"/>
<connect gate="PA" pin="PA14" pad="PA14"/>
<connect gate="PA" pin="PA15" pad="PA15"/>
<connect gate="PA" pin="PA2" pad="PA2"/>
<connect gate="PA" pin="PA3" pad="PA3"/>
<connect gate="PA" pin="PA4" pad="PA4"/>
<connect gate="PA" pin="PA5" pad="PA5"/>
<connect gate="PA" pin="PA6" pad="PA6"/>
<connect gate="PA" pin="PA7" pad="PA7"/>
<connect gate="PA" pin="PA9" pad="PA9"/>
<connect gate="PB" pin="PB0" pad="PB0"/>
<connect gate="PB" pin="PB1" pad="PB1"/>
<connect gate="PB" pin="PB3" pad="PB3"/>
<connect gate="PB" pin="PB4" pad="PB4"/>
<connect gate="PB" pin="PB5" pad="PB5"/>
<connect gate="PB" pin="PB6" pad="PB6"/>
<connect gate="PB" pin="PB7" pad="PB7"/>
<connect gate="PF" pin="!RST" pad="!RST"/>
<connect gate="PF" pin="BOOT0" pad="BOOT0"/>
<connect gate="PF" pin="OSC_IN" pad="OSC_IN"/>
<connect gate="PF" pin="OSC_OUT" pad="OSC_OUT"/>
<connect gate="PWR" pin="VDD" pad="VDD"/>
<connect gate="PWR" pin="VDDA" pad="VDDA"/>
<connect gate="PWR" pin="VDDIO2" pad="VDDIO2"/>
<connect gate="PWR" pin="VSS" pad="VSS"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12074869/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATA6561" prefix="IC">
<gates>
<gate name="ATA6561" symbol="ATA6561" x="-22.86" y="-7.62"/>
<gate name="PWR" symbol="ATA6561_PWR" x="-2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="SON65P300X300X90-9T170X250">
<connects>
<connect gate="ATA6561" pin="CANH" pad="7"/>
<connect gate="ATA6561" pin="CANL" pad="6"/>
<connect gate="ATA6561" pin="RXD" pad="4"/>
<connect gate="ATA6561" pin="STBY" pad="8"/>
<connect gate="ATA6561" pin="TXD" pad="1"/>
<connect gate="PWR" pin="VDD" pad="3"/>
<connect gate="PWR" pin="VDD1" pad="5"/>
<connect gate="PWR" pin="VSS" pad="2 9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11209438/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWD" prefix="SWD">
<gates>
<gate name="G$1" symbol="SWD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWD">
<connects>
<connect gate="G$1" pin="!RST" pad="RST"/>
<connect gate="G$1" pin="GND" pad="VSS"/>
<connect gate="G$1" pin="SWCLK" pad="SWC"/>
<connect gate="G$1" pin="SWD" pad="SWD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12074868/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADP1853" prefix="IC">
<gates>
<gate name="ADP1853" symbol="ADP1853" x="0" y="0"/>
<gate name="PWR" symbol="PWR-3" x="-30.48" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X80-21T275">
<connects>
<connect gate="ADP1853" pin="BST" pad="15"/>
<connect gate="ADP1853" pin="CLKOUT" pad="7"/>
<connect gate="ADP1853" pin="COMP" pad="4"/>
<connect gate="ADP1853" pin="CS" pad="12"/>
<connect gate="ADP1853" pin="DH" pad="14"/>
<connect gate="ADP1853" pin="DL" pad="11"/>
<connect gate="ADP1853" pin="ENB" pad="1"/>
<connect gate="ADP1853" pin="FB" pad="3"/>
<connect gate="ADP1853" pin="FREQ" pad="19"/>
<connect gate="ADP1853" pin="ILIM" pad="16"/>
<connect gate="ADP1853" pin="PGOOD" pad="17"/>
<connect gate="ADP1853" pin="RAMP" pad="18"/>
<connect gate="ADP1853" pin="SS" pad="2"/>
<connect gate="ADP1853" pin="SW" pad="13"/>
<connect gate="ADP1853" pin="SYNC" pad="6"/>
<connect gate="ADP1853" pin="TRK" pad="20"/>
<connect gate="ADP1853" pin="VCCO" pad="9"/>
<connect gate="PWR" pin="VDD" pad="8"/>
<connect gate="PWR" pin="VSS" pad="5 10 21"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11816611/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACS711" prefix="IC">
<gates>
<gate name="ACS711" symbol="ACS711" x="0" y="0" addlevel="request"/>
<gate name="PWR" symbol="PWR-2" x="-17.78" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="ACS711">
<connects>
<connect gate="ACS711" pin="!FAULT" pad="6"/>
<connect gate="ACS711" pin="IP+" pad="1-2"/>
<connect gate="ACS711" pin="IP-" pad="3-4"/>
<connect gate="ACS711" pin="VIOUT" pad="11"/>
<connect gate="PWR" pin="VDD" pad="12"/>
<connect gate="PWR" pin="VSS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077887/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="140120" package="L_140120">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077885/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="100115" package="L_100115">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077884/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NMOS" prefix="M" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS" x="-2.54" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="PG-TDSON-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077886/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BMS_PD">
<packages>
<package name="SON65P300X300X100-9T170X245" urn="urn:adsk.eagle:footprint:11906484/2">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 1.00 mm body, 2.45 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 1.00 mm and thermal pad size 2.45 X 1.70 mm&lt;/p&gt;</description>
<circle x="-2.054" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-1.55" y1="1.389" x2="-1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="1.389" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.389" x2="-1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.55" y2="-1.389" width="0.12" layer="21"/>
<smd name="1" x="-1.4346" y="0.975" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="2" x="-1.4346" y="0.325" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="3" x="-1.4346" y="-0.325" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="4" x="-1.4346" y="-0.975" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="5" x="1.4346" y="-0.975" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="6" x="1.4346" y="-0.325" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="7" x="1.4346" y="0.325" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="8" x="1.4346" y="0.975" dx="0.86" dy="0.32" layer="1" roundness="100"/>
<smd name="9" x="0" y="0" dx="1.7" dy="2.45" layer="1" thermals="no"/>
<text x="0" y="2.524" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.185" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
</package>
<package name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:footprint:11816613/2">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<circle x="-2.554" y="1.639" radius="0.25" width="0" layer="21"/>
<wire x1="-2.05" y1="1.389" x2="-2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="1.389" x2="2.05" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.389" y2="2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-1.389" x2="2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="1.389" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-1.389" x2="-2.05" y2="-2.05" width="0.12" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.389" y2="-2.05" width="0.12" layer="21"/>
<smd name="1" x="-1.9346" y="1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="2" x="-1.9346" y="0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="3" x="-1.9346" y="0" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="4" x="-1.9346" y="-0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="5" x="-1.9346" y="-1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="6" x="-1" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-0.5" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="0.5" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="1" y="-1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="1.9346" y="-1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="12" x="1.9346" y="-0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="13" x="1.9346" y="0" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="14" x="1.9346" y="0.5" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="15" x="1.9346" y="1" dx="0.8128" dy="0.27" layer="1" roundness="100"/>
<smd name="16" x="1" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.5" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="0" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="-0.5" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="-1" y="1.9346" dx="0.8128" dy="0.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="0" y="0" dx="2.75" dy="2.75" layer="1" thermals="no"/>
<text x="0" y="2.9996" size="0.6096" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9996" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="ACS711" urn="urn:adsk.eagle:footprint:12077855/1">
<smd name="1-2" x="-1.35" y="0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="3-4" x="-1.35" y="-0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="9" x="1.35" y="0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="1.35" y="-0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="7" x="1.35" y="-0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="1.35" y="0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="6" x="0.65" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="0.15" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.65" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.15" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-0.1875" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.9375" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.125" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.125" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="0.9375" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-0.1875" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.125" width="0.127" layer="21"/>
<circle x="-2.0625" y="1.875" radius="0.1875" width="0.127" layer="21"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="CAPC3216X160" urn="urn:adsk.eagle:footprint:11819575/2">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<smd name="1" x="-1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.2118" dy="1.7118" layer="1"/>
<text x="0" y="1.1699" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1699" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="21"/>
</package>
<package name="L_140120" urn="urn:adsk.eagle:footprint:12077853/1">
<smd name="P$1" x="-6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<smd name="P$2" x="6.5" y="0" dx="3.3" dy="4.9" layer="1"/>
<wire x1="-7" y1="6" x2="-7" y2="3" width="0.1524" layer="21"/>
<wire x1="-7" y1="6" x2="7" y2="6" width="0.1524" layer="21"/>
<wire x1="7" y1="6" x2="7" y2="3" width="0.1524" layer="21"/>
<wire x1="7" y1="-3" x2="7" y2="-7" width="0.1524" layer="21"/>
<wire x1="7" y1="-7" x2="-7" y2="-7" width="0.1524" layer="21"/>
<wire x1="-7" y1="-7" x2="-7" y2="-3" width="0.1524" layer="21"/>
<text x="-5.715" y="4.445" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-6.35" y="-6.35" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L_100115" urn="urn:adsk.eagle:footprint:12077881/1">
<smd name="P$1" x="-4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<smd name="P$2" x="4.75" y="0" dx="4" dy="3.5" layer="1" rot="R90"/>
<wire x1="-6" y1="2.25" x2="-6" y2="5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="5.25" x2="6" y2="5.25" width="0.127" layer="21"/>
<wire x1="6" y1="5.25" x2="6" y2="2.25" width="0.127" layer="21"/>
<wire x1="6" y1="-2.25" x2="6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="6" y1="-5.25" x2="-6" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-6" y1="-5.25" x2="-6" y2="-2.25" width="0.127" layer="21"/>
<text x="-5.25" y="4.25" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-5.5" y="-4.75" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="PG-TDSON-8" urn="urn:adsk.eagle:footprint:12077854/1">
<smd name="3" x="2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="1" x="2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="4" x="2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="6" x="-2.825" y="0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="7" x="-2.825" y="-0.635" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="8" x="-2.825" y="-1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<smd name="5" x="-2.825" y="1.905" dx="0.75" dy="1.3" layer="1" rot="R90"/>
<polygon width="0.002540625" layer="1">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="2.285"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.475" y="-2.28"/>
<vertex x="-3.475" y="-1.53"/>
<vertex x="-2.175" y="-1.53"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-3.475" y="-1.01"/>
<vertex x="-2.175" y="-1.01"/>
<vertex x="-2.175" y="-0.26"/>
<vertex x="-2.175" y="0.26"/>
<vertex x="-3.475" y="0.26"/>
<vertex x="-3.475" y="1.01"/>
<vertex x="-2.175" y="1.01"/>
<vertex x="-2.175" y="1.53"/>
<vertex x="-3.475" y="1.53"/>
<vertex x="-3.475" y="2.28"/>
<vertex x="1.625" y="2.28"/>
<vertex x="1.625" y="-2.28"/>
</polygon>
<circle x="3.81" y="-2.8575" radius="0.3175" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.54" x2="2.8575" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.8575" y1="-2.54" x2="-3.4925" y2="-2.54" width="0.127" layer="21"/>
<text x="-3.4925" y="2.8575" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-3.4925" y="-3.4925" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="POWERDI3333-8" urn="urn:adsk.eagle:footprint:12077882/1">
<smd name="3" x="-1.5" y="-0.325" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="-1.5" y="0.325" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="1" x="-1.5" y="0.975" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-1.5" y="-0.975" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="6" x="1.5" y="-0.325" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="1.5" y="0.325" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="1.5" y="0.975" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<smd name="5" x="1.5" y="-0.975" dx="0.42" dy="0.7" layer="1" rot="R90"/>
<polygon width="0" layer="1">
<vertex x="1.15" y="1.185"/>
<vertex x="1.15" y="-0.765"/>
<vertex x="1.85" y="-0.765"/>
<vertex x="1.85" y="-1.185"/>
<vertex x="-0.4" y="-1.185"/>
<vertex x="-0.4" y="-0.765"/>
<vertex x="-0.4" y="1.185"/>
</polygon>
<wire x1="-1.666875" y1="1.666875" x2="1.74625" y2="1.666875" width="0.127" layer="21"/>
<wire x1="-1.666875" y1="-1.666875" x2="1.74625" y2="-1.666875" width="0.127" layer="21"/>
<circle x="-2.143125" y="1.666875" radius="0.15875" width="0.127" layer="21"/>
<text x="-1.666875" y="1.825625" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.666875" y="-2.460625" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SOT95P230X110-3" urn="urn:adsk.eagle:footprint:11924659/2">
<description>3-SOT23, 0.95 mm pitch, 2.30 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.30 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<circle x="-1.204" y="1.749" radius="0.25" width="0" layer="21"/>
<wire x1="-0.7" y1="1.559" x2="0.7" y2="1.559" width="0.12" layer="21"/>
<wire x1="0.7" y1="1.559" x2="0.7" y2="0.549" width="0.12" layer="21"/>
<wire x1="-0.7" y1="-1.559" x2="0.7" y2="-1.559" width="0.12" layer="21"/>
<wire x1="0.7" y1="-1.559" x2="0.7" y2="-0.549" width="0.12" layer="21"/>
<smd name="1" x="-1.0783" y="0.95" dx="1.0588" dy="0.59" layer="1"/>
<smd name="2" x="-1.0783" y="-0.95" dx="1.0588" dy="0.59" layer="1"/>
<smd name="3" x="1.0783" y="0" dx="1.0588" dy="0.59" layer="1"/>
<text x="0" y="2.634" size="0.6096" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.194" size="0.6096" layer="27" font="vector" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SON65P300X300X100-9T170X245" urn="urn:adsk.eagle:package:11906480/2" type="model">
<description>8-SON, 0.65 mm pitch, 3.00 X 3.00 X 1.00 mm body, 2.45 X 1.70 mm thermal pad
&lt;p&gt;8-pin SON package with 0.65 mm pitch with body size 3.00 X 3.00 X 1.00 mm and thermal pad size 2.45 X 1.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SON65P300X300X100-9T170X245"/>
</packageinstances>
</package3d>
<package3d name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:package:11816611/2" type="model">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN50P400X400X80-21T275"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X160" urn="urn:adsk.eagle:package:11819311/2" type="model">
<description>Chip, 3.20 X 1.60 X 1.60 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 1.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3216X160"/>
</packageinstances>
</package3d>
<package3d name="SOT95P230X110-3" urn="urn:adsk.eagle:package:11924654/2" type="model">
<description>3-SOT23, 0.95 mm pitch, 2.30 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.30 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P230X110-3"/>
</packageinstances>
</package3d>
<package3d name="ACS711" urn="urn:adsk.eagle:package:12077887/3" type="model">
<packageinstances>
<packageinstance name="ACS711"/>
</packageinstances>
</package3d>
<package3d name="L_140120" urn="urn:adsk.eagle:package:12077885/2" type="model">
<packageinstances>
<packageinstance name="L_140120"/>
</packageinstances>
</package3d>
<package3d name="L_100115" urn="urn:adsk.eagle:package:12077884/2" type="model">
<packageinstances>
<packageinstance name="L_100115"/>
</packageinstances>
</package3d>
<package3d name="PG-TDSON-8" urn="urn:adsk.eagle:package:12077886/2" type="model">
<packageinstances>
<packageinstance name="PG-TDSON-8"/>
</packageinstances>
</package3d>
<package3d name="POWERDI3333-8" urn="urn:adsk.eagle:package:12077883/2" type="model">
<packageinstances>
<packageinstance name="POWERDI3333-8"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PWR" urn="urn:adsk.eagle:symbol:12077876/1">
<pin name="VDD" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<circle x="0" y="3.048" radius="1.016" width="0.1524" layer="94"/>
<circle x="0" y="-3.048" radius="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-4.064" width="0.1524" layer="94"/>
<text x="-0.635" y="5.08" size="0.8128" layer="95" font="vector" rot="R90">VDD</text>
<text x="-0.635" y="-5.08" size="0.8128" layer="95" font="vector" rot="R90" align="bottom-right">VSS</text>
<text x="-2.54" y="0" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="VBAT" urn="urn:adsk.eagle:symbol:12077866/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VBAT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="BQ29200" urn="urn:adsk.eagle:symbol:12077864/1">
<pin name="OUT" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="CD" x="-12.7" y="-5.08" length="short"/>
<pin name="!CB_EN" x="-12.7" y="-2.54" length="short"/>
<pin name="VC1_CB" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="VC1" x="-12.7" y="2.54" length="short"/>
<pin name="VC2" x="-12.7" y="5.08" length="short"/>
<wire x1="-10.16" y1="6.35" x2="-10.16" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-6.35" x2="10.16" y2="-6.35" width="0.254" layer="94"/>
<wire x1="10.16" y1="-6.35" x2="10.16" y2="6.35" width="0.254" layer="94"/>
<wire x1="10.16" y1="6.35" x2="-10.16" y2="6.35" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="VBATS2" urn="urn:adsk.eagle:symbol:12077863/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VBATS2" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="ADP1853" urn="urn:adsk.eagle:symbol:12077871/1">
<pin name="ENB" x="-12.7" y="15.24" visible="pin" length="short"/>
<pin name="TRK" x="-12.7" y="12.7" visible="pin" length="short"/>
<pin name="VCCO" x="-12.7" y="10.16" visible="pin" length="short"/>
<pin name="SYNC" x="-12.7" y="2.54" visible="pin" length="short"/>
<pin name="FREQ" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="SS" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="RAMP" x="-12.7" y="17.78" visible="pin" length="short"/>
<pin name="DH" x="12.7" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="BST" x="12.7" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="SW" x="12.7" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="CS" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="ILIM" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DL" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="FB" x="12.7" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="COMP" x="12.7" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="PGOOD" x="12.7" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="CLKOUT" x="12.7" y="-20.32" visible="pin" length="short" rot="R180"/>
<wire x1="-10.16" y1="19.05" x2="-10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-21.59" x2="10.16" y2="-21.59" width="0.254" layer="94"/>
<wire x1="10.16" y1="-21.59" x2="10.16" y2="19.05" width="0.254" layer="94"/>
<wire x1="10.16" y1="19.05" x2="-10.16" y2="19.05" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="ACS711" urn="urn:adsk.eagle:symbol:12077870/1">
<pin name="IP+" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="IP-" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="VIOUT" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="!FAULT" x="10.16" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="C" urn="urn:adsk.eagle:symbol:12077867/1">
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="-1.905" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="1.905" y2="0.635" width="0.3048" layer="94"/>
<text x="2.54" y="-1.905" size="0.8128" layer="95" rot="R90" align="top-left">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="95" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="L" urn="urn:adsk.eagle:symbol:12077868/1">
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="95" align="top-left">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.9525" x2="2.54" y2="0.9525" layer="94"/>
</symbol>
<symbol name="NMOS" urn="urn:adsk.eagle:symbol:12077869/1">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="6.35" y="-1.27" size="0.8128" layer="96" rot="MR270" align="bottom-right">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
</symbol>
<symbol name="PGND" urn="urn:adsk.eagle:symbol:12077865/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="PMOS" urn="urn:adsk.eagle:symbol:12077861/1">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-0.1905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.1905" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.318" y1="-0.1905" x2="3.81" y2="-0.1905" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.1905" x2="3.302" y2="-0.1905" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="6.35" y="-1.27" size="0.8128" layer="96" rot="MR270" align="bottom-right">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="2.4765" y="0"/>
<vertex x="1.4605" y="-0.508"/>
<vertex x="1.4605" y="0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="-0.1905"/>
<vertex x="4.318" y="0.5715"/>
<vertex x="3.302" y="0.5715"/>
</polygon>
</symbol>
<symbol name="NPN" urn="urn:adsk.eagle:symbol:12077862/1">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="5.08" y="2.54" size="0.8128" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="VOUT" urn="urn:adsk.eagle:symbol:12077859/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VOUT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VBAT" urn="urn:adsk.eagle:component:12077898/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VBAT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ29200" urn="urn:adsk.eagle:component:12077896/1" prefix="IC">
<gates>
<gate name="PWR" symbol="PWR" x="-45.72" y="2.54" addlevel="request"/>
<gate name="BQ29200" symbol="BQ29200" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="VSON" package="SON65P300X300X100-9T170X245">
<connects>
<connect gate="BQ29200" pin="!CB_EN" pad="6"/>
<connect gate="BQ29200" pin="CD" pad="4"/>
<connect gate="BQ29200" pin="OUT" pad="8"/>
<connect gate="BQ29200" pin="VC1" pad="2"/>
<connect gate="BQ29200" pin="VC1_CB" pad="3"/>
<connect gate="BQ29200" pin="VC2" pad="1"/>
<connect gate="PWR" pin="VDD" pad="7"/>
<connect gate="PWR" pin="VSS" pad="5 9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11906480/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VBATS2" urn="urn:adsk.eagle:component:12077895/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VBATS2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADP1853" urn="urn:adsk.eagle:component:12077903/1" prefix="IC">
<gates>
<gate name="ADP1853" symbol="ADP1853" x="0" y="0"/>
<gate name="PWR" symbol="PWR" x="-30.48" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X80-21T275">
<connects>
<connect gate="ADP1853" pin="BST" pad="15"/>
<connect gate="ADP1853" pin="CLKOUT" pad="7"/>
<connect gate="ADP1853" pin="COMP" pad="4"/>
<connect gate="ADP1853" pin="CS" pad="12"/>
<connect gate="ADP1853" pin="DH" pad="14"/>
<connect gate="ADP1853" pin="DL" pad="11"/>
<connect gate="ADP1853" pin="ENB" pad="1"/>
<connect gate="ADP1853" pin="FB" pad="3"/>
<connect gate="ADP1853" pin="FREQ" pad="19"/>
<connect gate="ADP1853" pin="ILIM" pad="16"/>
<connect gate="ADP1853" pin="PGOOD" pad="17"/>
<connect gate="ADP1853" pin="RAMP" pad="18"/>
<connect gate="ADP1853" pin="SS" pad="2"/>
<connect gate="ADP1853" pin="SW" pad="13"/>
<connect gate="ADP1853" pin="SYNC" pad="6"/>
<connect gate="ADP1853" pin="TRK" pad="20"/>
<connect gate="ADP1853" pin="VCCO" pad="9"/>
<connect gate="PWR" pin="VDD" pad="8"/>
<connect gate="PWR" pin="VSS" pad="5 10 21"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11816611/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACS711" urn="urn:adsk.eagle:component:12077902/1" locally_modified="yes" prefix="IC">
<gates>
<gate name="ACS711" symbol="ACS711" x="0" y="0" addlevel="request"/>
<gate name="PWR" symbol="PWR" x="-17.78" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="ACS711">
<connects>
<connect gate="ACS711" pin="!FAULT" pad="6"/>
<connect gate="ACS711" pin="IP+" pad="1-2"/>
<connect gate="ACS711" pin="IP-" pad="3-4"/>
<connect gate="ACS711" pin="VIOUT" pad="11"/>
<connect gate="PWR" pin="VDD" pad="12"/>
<connect gate="PWR" pin="VSS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077887/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" urn="urn:adsk.eagle:component:12077899/1" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC3216X160">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11819311/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" urn="urn:adsk.eagle:component:12077900/1" locally_modified="yes" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="140120" package="L_140120">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077885/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="100115" package="L_100115">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077884/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NMOS" urn="urn:adsk.eagle:component:12077901/1" locally_modified="yes" prefix="M" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS" x="-2.54" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="PG-TDSON-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077886/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PGND" urn="urn:adsk.eagle:component:12077897/1" prefix="PGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PMOS" urn="urn:adsk.eagle:component:12077892/1" locally_modified="yes" prefix="M" uservalue="yes">
<gates>
<gate name="G$1" symbol="PMOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERDI3333-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12077883/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NPN" urn="urn:adsk.eagle:component:12077893/1" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT95P230X110-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:11924654/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VOUT" urn="urn:adsk.eagle:component:12077891/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VOUT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME6" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="FRAME7" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="FRAME8" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC11" library="BMS_PD" deviceset="BQ29200" device="VSON" package3d_urn="urn:adsk.eagle:package:11906480/2"/>
<part name="R34" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="160"/>
<part name="R35" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="260"/>
<part name="R36" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="100"/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C50" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="P+25" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="P+26" library="BMS_PD" deviceset="VBATS2" device=""/>
<part name="FRAME2" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC3" library="Standard" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R7" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="R8" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="R9" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="33k"/>
<part name="R10" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="8k6"/>
<part name="R11" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="18k"/>
<part name="C10" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="390pF"/>
<part name="C11" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="16pF"/>
<part name="IC4" library="Standard" deviceset="ACS711" device="" package3d_urn="urn:adsk.eagle:package:12077887/3"/>
<part name="R12" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="820k"/>
<part name="C12" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="10nF"/>
<part name="C13" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C14" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="22nF"/>
<part name="C15" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L2" library="Standard" deviceset="L" device="140120" package3d_urn="urn:adsk.eagle:package:12077885/2" value="10µH"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="M3" library="Standard" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M4" library="Standard" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="C16" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C17" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C18" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="P+8" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND3" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND4" library="BMS_PD" deviceset="PGND" device=""/>
<part name="FRAME4" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC7" library="Standard" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R19" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="R20" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="R21" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="33k"/>
<part name="R22" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="8k6"/>
<part name="R23" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="18k"/>
<part name="C28" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="390pF"/>
<part name="C29" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="16pF"/>
<part name="IC8" library="BMS_PD" deviceset="ACS711" device="" package3d_urn="urn:adsk.eagle:package:12077887/3"/>
<part name="R24" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="820k"/>
<part name="C30" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="10nF"/>
<part name="C31" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C32" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="22nF"/>
<part name="C33" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L4" library="BMS_PD" deviceset="L" device="140120" package3d_urn="urn:adsk.eagle:package:12077885/2" value="10µH"/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="M7" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M8" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="C34" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C35" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C36" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="P+16" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND7" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND8" library="BMS_PD" deviceset="PGND" device=""/>
<part name="FRAME9" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC12" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="P+21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R31" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="R32" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="R33" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="33k"/>
<part name="R37" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="8k6"/>
<part name="R38" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="18k"/>
<part name="C46" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="390pF"/>
<part name="C47" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="16pF"/>
<part name="IC13" library="BMS_PD" deviceset="ACS711" device="" package3d_urn="urn:adsk.eagle:package:12077887/3"/>
<part name="R39" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="820k"/>
<part name="C48" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="10nF"/>
<part name="C49" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C51" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="22nF"/>
<part name="C52" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L6" library="BMS_PD" deviceset="L" device="140120" package3d_urn="urn:adsk.eagle:package:12077885/2" value="10µH"/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND42" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="M11" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M12" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="C53" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C54" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="GND43" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C55" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="P+24" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND11" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND12" library="BMS_PD" deviceset="PGND" device=""/>
<part name="FRAME11" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC16" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="P+31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND52" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R46" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="R47" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="R48" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="33k"/>
<part name="R49" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="8k6"/>
<part name="R50" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="18k"/>
<part name="C65" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="390pF"/>
<part name="C66" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="16pF"/>
<part name="IC17" library="BMS_PD" deviceset="ACS711" device="" package3d_urn="urn:adsk.eagle:package:12077887/3"/>
<part name="R51" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="820k"/>
<part name="C67" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="10nF"/>
<part name="C68" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C69" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="22nF"/>
<part name="C70" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L8" library="BMS_PD" deviceset="L" device="140120" package3d_urn="urn:adsk.eagle:package:12077885/2" value="10µH"/>
<part name="GND53" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND54" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND55" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND56" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="M15" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M16" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="C71" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C72" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="GND57" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND58" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C73" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="P+34" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND15" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND16" library="BMS_PD" deviceset="PGND" device=""/>
<part name="FRAME13" library="Standard" deviceset="A4L-LOC" device=""/>
<part name="IC20" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="R60" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="264k"/>
<part name="R61" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="10k"/>
<part name="R62" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="R63" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="R64" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="13k"/>
<part name="C87" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C88" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="10nF"/>
<part name="C89" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C90" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="C91" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="C92" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L11" library="BMS_PD" deviceset="L" device="100115" package3d_urn="urn:adsk.eagle:package:12077884/2" value="1µH"/>
<part name="M21" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M22" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="P+43" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND23" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND24" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND25" library="BMS_PD" deviceset="PGND" device=""/>
<part name="GND67" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND68" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND69" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND70" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+45" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="IC21" library="BMS_PD" deviceset="ADP1853" device="" package3d_urn="urn:adsk.eagle:package:11816611/2"/>
<part name="R65" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="264k"/>
<part name="R66" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="10k"/>
<part name="R67" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="22k"/>
<part name="C93" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C94" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C95" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="C96" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="C97" library="BMS_PD" deviceset="C" device="" package3d_urn="urn:adsk.eagle:package:11819311/2" value="100µF"/>
<part name="L12" library="BMS_PD" deviceset="L" device="100115" package3d_urn="urn:adsk.eagle:package:12077884/2" value="1µH"/>
<part name="M23" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="M24" library="BMS_PD" deviceset="NMOS" device="" package3d_urn="urn:adsk.eagle:package:12077886/2" value="BSC010NE2LS"/>
<part name="P+46" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="PGND26" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND27" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND28" library="BMS_PD" deviceset="PGND" device=""/>
<part name="GND71" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND72" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+47" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+48" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+49" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+50" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C98" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C99" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="GND73" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND74" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="M1" library="BMS_PD" deviceset="PMOS" device="" package3d_urn="urn:adsk.eagle:package:12077883/2" value="DMP2005UFG-7"/>
<part name="R1" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="10k"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="P+2" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="UREG1" library="Standard" deviceset="UREG" device="" package3d_urn="urn:adsk.eagle:package:11372008/2" value="5V"/>
<part name="UREG2" library="Standard" deviceset="UREG" device="" package3d_urn="urn:adsk.eagle:package:11372008/2" value="3V3"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C2" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C3" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C4" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C5" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C6" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="C8" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="100nF"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R2" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="10k"/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC1" library="Standard" deviceset="ATA6561" device="" package3d_urn="urn:adsk.eagle:package:11209438/2"/>
<part name="MCU1" library="Standard" deviceset="STM32F042G6" device="" package3d_urn="urn:adsk.eagle:package:12074869/2"/>
<part name="SWD1" library="Standard" deviceset="SWD" device="" package3d_urn="urn:adsk.eagle:package:12074868/1"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="PGND2" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND5" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND6" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND9" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND10" library="BMS_PD" deviceset="PGND" device=""/>
<part name="PGND1" library="BMS_PD" deviceset="PGND" device=""/>
<part name="P+19" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="C9" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C19" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C20" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C21" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="C22" library="Standard" deviceset="C" device="0402" package3d_urn="urn:adsk.eagle:package:11214499/2" value="1µF"/>
<part name="VIN" library="Standard" deviceset="X4" device="SMD_COM" package3d_urn="urn:adsk.eagle:package:12077989/1" value="X4SMD_COM"/>
<part name="M2" library="BMS_PD" deviceset="PMOS" device="" package3d_urn="urn:adsk.eagle:package:12077883/2" value="DMP2005UFG-7"/>
<part name="R3" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="10k"/>
<part name="Q1" library="BMS_PD" deviceset="NPN" device="SOT23" package3d_urn="urn:adsk.eagle:package:11924654/2" value="MMBT3904"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+18" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="R4" library="Standard" deviceset="R" device="0402" package3d_urn="urn:adsk.eagle:package:11214492/2" value="1k"/>
<part name="P+9" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="P+10" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="P+11" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="P+12" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="P+17" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="P+20" library="BMS_PD" deviceset="VBAT" device=""/>
<part name="P+27" library="BMS_PD" deviceset="VOUT" device=""/>
<part name="PGND13" library="BMS_PD" deviceset="PGND" device=""/>
<part name="BAT" library="Standard" deviceset="X3" device="SMDCOM" package3d_urn="urn:adsk.eagle:package:12077988/1"/>
<part name="P+3" library="BMS_PD" deviceset="VBATS2" device=""/>
<part name="FOOT_R" library="Standard" deviceset="X4" device="THT" package3d_urn="urn:adsk.eagle:package:12077990/3"/>
<part name="FOOT_L" library="Standard" deviceset="X4" device="THT" package3d_urn="urn:adsk.eagle:package:12077990/3"/>
<part name="ARM_R" library="Standard" deviceset="X4" device="THT" package3d_urn="urn:adsk.eagle:package:12077990/3"/>
<part name="ARM_L" library="Standard" deviceset="X4" device="THT" package3d_urn="urn:adsk.eagle:package:12077990/3"/>
<part name="HEAD" library="Standard" deviceset="X4" device="THT" package3d_urn="urn:adsk.eagle:package:12077990/3"/>
<part name="GND" library="Standard" deviceset="X2" device="SMD" package3d_urn="urn:adsk.eagle:package:12077992/1"/>
<part name="PGND14" library="BMS_PD" deviceset="PGND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">Microcontroller, CAN-Bus
and Programmingline</text>
</plain>
<instances>
<instance part="FRAME6" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="UREG1" gate="G$1" x="48.26" y="165.1" smashed="yes">
<attribute name="NAME" x="43.18" y="168.91" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="53.34" y="160.02" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="UREG2" gate="G$1" x="48.26" y="132.08" smashed="yes">
<attribute name="NAME" x="43.18" y="135.89" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="53.34" y="127" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="GND7" gate="1" x="38.1" y="139.7" smashed="yes">
<attribute name="VALUE" x="35.56" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="58.42" y="139.7" smashed="yes">
<attribute name="VALUE" x="55.88" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="50.8" y="139.7" smashed="yes">
<attribute name="VALUE" x="48.26" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="45.72" y="139.7" smashed="yes">
<attribute name="VALUE" x="43.18" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="50.8" y="106.68" smashed="yes">
<attribute name="VALUE" x="48.26" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="58.42" y="106.68" smashed="yes">
<attribute name="VALUE" x="55.88" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="33.02" y="106.68" smashed="yes">
<attribute name="VALUE" x="30.48" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="33.02" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="31.115" y="126.365" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="34.925" y="126.365" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C3" gate="G$1" x="58.42" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="56.515" y="126.365" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="60.325" y="126.365" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C4" gate="G$1" x="58.42" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="56.515" y="159.385" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="60.325" y="159.385" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C5" gate="G$1" x="38.1" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="36.195" y="159.385" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="40.005" y="159.385" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="+3V1" gate="G$1" x="96.52" y="137.16" smashed="yes">
<attribute name="VALUE" x="93.98" y="132.08" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="81.28" y="106.68" smashed="yes">
<attribute name="VALUE" x="78.74" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="91.44" y="124.46" smashed="yes">
<attribute name="NAME" x="93.345" y="122.555" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="89.535" y="122.555" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="71.12" y="170.18" smashed="yes">
<attribute name="VALUE" x="68.58" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="73.66" y="139.7" smashed="yes">
<attribute name="VALUE" x="71.12" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="66.04" y="157.48" smashed="yes">
<attribute name="NAME" x="67.945" y="155.575" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="64.135" y="155.575" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="81.28" y="157.48" smashed="yes">
<attribute name="NAME" x="83.185" y="155.575" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="79.375" y="155.575" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="76.2" y="170.18" smashed="yes">
<attribute name="VALUE" x="73.66" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="58.42" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="57.15" y="81.28" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="59.69" y="81.28" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="+3V5" gate="G$1" x="58.42" y="93.98" smashed="yes">
<attribute name="VALUE" x="55.88" y="88.9" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND29" gate="1" x="121.92" y="83.82" smashed="yes">
<attribute name="VALUE" x="119.38" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="ATA6561" x="134.62" y="83.82" smashed="yes">
<attribute name="NAME" x="127" y="91.44" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC1" gate="PWR" x="73.66" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="76.2" y="154.94" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="MCU1" gate="PF" x="43.18" y="81.28" smashed="yes">
<attribute name="NAME" x="31.75" y="86.36" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="MCU1" gate="PA" x="43.18" y="48.26" smashed="yes">
<attribute name="NAME" x="36.83" y="66.04" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="MCU1" gate="PWR" x="81.28" y="121.92" smashed="yes"/>
<instance part="SWD1" gate="G$1" x="104.14" y="38.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="111.76" y="45.72" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="GND31" gate="1" x="96.52" y="27.94" smashed="yes">
<attribute name="VALUE" x="93.98" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="96.52" y="50.8" smashed="yes">
<attribute name="VALUE" x="93.98" y="45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="MCU1" gate="PB" x="43.18" y="17.78" smashed="yes">
<attribute name="NAME" x="36.83" y="27.94" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+20" gate="VCC" x="33.02" y="137.16" smashed="yes">
<attribute name="VALUE" x="30.48" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+27" gate="G$1" x="38.1" y="170.18" smashed="yes">
<attribute name="VALUE" x="35.56" y="167.64" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
<bus name="BMS:BAL,CC#[0..3],CV#0">
<segment>
<wire x1="53.34" y1="63.5" x2="53.34" y2="50.8" width="0.762" layer="92"/>
<label x="55.88" y="55.88" size="1.778" layer="95" font="vector"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="UREG1" gate="G$1" pin="EN"/>
<wire x1="45.72" y1="142.24" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="UREG1" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="142.24" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="UREG2" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="109.22" x2="50.8" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="P$1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="38.1" y1="152.4" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="P$1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="58.42" y1="152.4" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="P$1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="58.42" y1="119.38" x2="58.42" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="109.22" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="P$2"/>
<wire x1="81.28" y1="111.76" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="81.28" y1="111.76" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<wire x1="91.44" y1="111.76" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<junction x="81.28" y="111.76"/>
<pinref part="MCU1" gate="PWR" pin="VSS"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="66.04" y1="152.4" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<wire x1="66.04" y1="144.78" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="73.66" y1="144.78" x2="73.66" y2="142.24" width="0.1524" layer="91"/>
<wire x1="73.66" y1="144.78" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<junction x="73.66" y="144.78"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<wire x1="73.66" y1="144.78" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<wire x1="81.28" y1="144.78" x2="81.28" y2="152.4" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VSS"/>
</segment>
<segment>
<wire x1="124.46" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="STBY"/>
</segment>
<segment>
<pinref part="SWD1" gate="G$1" pin="GND"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="99.06" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="96.52" y1="35.56" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="76.2" y1="132.08" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="132.08" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="132.08" x2="91.44" y2="132.08" width="0.1524" layer="91"/>
<wire x1="91.44" y1="132.08" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<wire x1="96.52" y1="132.08" x2="96.52" y2="134.62" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="129.54" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<junction x="86.36" y="132.08"/>
<wire x1="81.28" y1="129.54" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<junction x="81.28" y="132.08"/>
<wire x1="76.2" y1="129.54" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<junction x="76.2" y="132.08"/>
<pinref part="C6" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="129.54" x2="91.44" y2="132.08" width="0.1524" layer="91"/>
<junction x="91.44" y="132.08"/>
<pinref part="MCU1" gate="PWR" pin="VDD"/>
<pinref part="MCU1" gate="PWR" pin="VDDIO2"/>
<pinref part="MCU1" gate="PWR" pin="VDDA"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="58.42" y1="129.54" x2="58.42" y2="132.08" width="0.1524" layer="91"/>
<pinref part="UREG2" gate="G$1" pin="OUT"/>
<wire x1="58.42" y1="132.08" x2="55.88" y2="132.08" width="0.1524" layer="91"/>
<wire x1="58.42" y1="132.08" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<junction x="58.42" y="132.08"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="P$1"/>
<wire x1="81.28" y1="162.56" x2="81.28" y2="165.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="165.1" x2="76.2" y2="165.1" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="76.2" y1="165.1" x2="76.2" y2="167.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VDD1"/>
<wire x1="76.2" y1="162.56" x2="76.2" y2="165.1" width="0.1524" layer="91"/>
<junction x="76.2" y="165.1"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="P$2"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="88.9" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SWD1" gate="G$1" pin="VCC"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="99.06" y1="43.18" x2="96.52" y2="43.18" width="0.1524" layer="91"/>
<wire x1="96.52" y1="43.18" x2="96.52" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="58.42" y1="165.1" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<wire x1="66.04" y1="165.1" x2="71.12" y2="165.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="165.1" x2="71.12" y2="167.64" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<junction x="66.04" y="165.1"/>
<pinref part="UREG1" gate="G$1" pin="OUT"/>
<wire x1="55.88" y1="165.1" x2="58.42" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="P$2"/>
<wire x1="58.42" y1="162.56" x2="58.42" y2="165.1" width="0.1524" layer="91"/>
<junction x="58.42" y="165.1"/>
<pinref part="IC1" gate="PWR" pin="VDD"/>
<wire x1="71.12" y1="162.56" x2="71.12" y2="165.1" width="0.1524" layer="91"/>
<junction x="71.12" y="165.1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$1"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="58.42" y1="76.2" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PF" pin="!RST"/>
<wire x1="58.42" y1="76.2" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="58.42" y="76.2"/>
<pinref part="SWD1" gate="G$1" pin="!RST"/>
<wire x1="93.98" y1="33.02" x2="99.06" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="124.46" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="63.5" y1="81.28" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="63.5" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="TXD"/>
<pinref part="MCU1" gate="PA" pin="PA10"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="48.26" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<wire x1="66.04" y1="78.74" x2="124.46" y2="78.74" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="RXD"/>
<pinref part="MCU1" gate="PA" pin="PA9"/>
</segment>
</net>
<net name="CAN_H" class="0">
<segment>
<wire x1="144.78" y1="88.9" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="CANH"/>
<label x="154.94" y="88.9" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="CAN_L" class="0">
<segment>
<wire x1="154.94" y1="78.74" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ATA6561" pin="CANL"/>
<label x="154.94" y="78.74" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="SWD1" gate="G$1" pin="SWD"/>
<pinref part="MCU1" gate="PA" pin="PA13"/>
<wire x1="99.06" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="SWD1" gate="G$1" pin="SWCLK"/>
<wire x1="99.06" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PA" pin="PA14"/>
<wire x1="66.04" y1="35.56" x2="48.26" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CC#0" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA0"/>
<wire x1="48.26" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CC#1" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA1"/>
<wire x1="48.26" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CC#2" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA2"/>
<wire x1="48.26" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CC#3" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA3"/>
<wire x1="48.26" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CV#0" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA4"/>
<wire x1="48.26" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAL" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA5"/>
<wire x1="48.26" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT_COM1" class="0">
<segment>
<pinref part="MCU1" gate="PB" pin="PB6"/>
<wire x1="48.26" y1="12.7" x2="68.58" y2="12.7" width="0.1524" layer="91"/>
<label x="68.58" y="12.7" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="BAT_COM2" class="0">
<segment>
<pinref part="MCU1" gate="PB" pin="PB7"/>
<wire x1="48.26" y1="10.16" x2="60.96" y2="10.16" width="0.1524" layer="91"/>
<wire x1="60.96" y1="10.16" x2="60.96" y2="7.62" width="0.1524" layer="91"/>
<wire x1="60.96" y1="7.62" x2="68.58" y2="7.62" width="0.1524" layer="91"/>
<label x="68.58" y="7.62" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="BAT_CTRL" class="0">
<segment>
<pinref part="MCU1" gate="PA" pin="PA6"/>
<wire x1="48.26" y1="48.26" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<label x="68.58" y="48.26" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="UREG1" gate="G$1" pin="IN"/>
<wire x1="40.64" y1="165.1" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="165.1" x2="38.1" y2="162.56" width="0.1524" layer="91"/>
<pinref part="P+27" gate="G$1" pin="VOUT"/>
<wire x1="38.1" y1="165.1" x2="38.1" y2="167.64" width="0.1524" layer="91"/>
<junction x="38.1" y="165.1"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="P+20" gate="VCC" pin="VBAT"/>
<pinref part="UREG2" gate="G$1" pin="IN"/>
<pinref part="UREG2" gate="G$1" pin="EN"/>
<wire x1="38.1" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="127" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="45.72" y1="121.92" x2="38.1" y2="121.92" width="0.1524" layer="91"/>
<wire x1="38.1" y1="121.92" x2="38.1" y2="132.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="132.08" x2="33.02" y2="132.08" width="0.1524" layer="91"/>
<junction x="38.1" y="132.08"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<wire x1="33.02" y1="132.08" x2="33.02" y2="129.54" width="0.1524" layer="91"/>
<junction x="33.02" y="132.08"/>
<wire x1="33.02" y1="134.62" x2="33.02" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">CCLane#0
Delivers 10A to charge the battery</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC3" gate="ADP1853" x="104.14" y="81.28" smashed="yes">
<attribute name="NAME" x="93.98" y="101.6" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+5" gate="VCC" x="88.9" y="119.38" smashed="yes">
<attribute name="VALUE" x="86.36" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="71.12" y="50.8" smashed="yes">
<attribute name="VALUE" x="68.58" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="124.46" y="86.36" smashed="yes">
<attribute name="NAME" x="121.92" y="87.63" size="0.8128" layer="95"/>
<attribute name="VALUE" x="121.92" y="85.09" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R8" gate="G$1" x="127" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="129.54" y="72.39" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="129.54" y="74.93" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="R9" gate="G$1" x="142.24" y="68.58" smashed="yes">
<attribute name="NAME" x="139.7" y="69.85" size="0.8128" layer="95"/>
<attribute name="VALUE" x="139.7" y="67.31" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R10" gate="G$1" x="167.64" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="73.66" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="73.66" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="R11" gate="G$1" x="167.64" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="58.42" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="58.42" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C10" gate="G$1" x="129.54" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="131.445" y="70.485" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="131.445" y="66.675" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C11" gate="G$1" x="132.08" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="133.985" y="66.04" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="133.985" y="60.96" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="IC4" gate="ACS711" x="185.42" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="193.04" y="86.36" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="R12" gate="G$1" x="88.9" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="104.14" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="104.14" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C12" gate="G$1" x="71.12" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="68.58" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="73.66" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C13" gate="G$1" x="78.74" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="81.28" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C14" gate="G$1" x="124.46" y="96.52" smashed="yes" rot="R270">
<attribute name="NAME" x="122.555" y="93.98" size="0.8128" layer="95" align="top-left"/>
<attribute name="VALUE" x="122.555" y="99.06" size="0.8128" layer="95"/>
</instance>
<instance part="C15" gate="G$1" x="208.28" y="83.82" smashed="yes">
<attribute name="NAME" x="210.82" y="81.915" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="205.74" y="81.915" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="L2" gate="G$1" x="180.34" y="91.44" smashed="yes">
<attribute name="NAME" x="177.8" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="177.8" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="GND5" gate="1" x="78.74" y="50.8" smashed="yes">
<attribute name="VALUE" x="76.2" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="86.36" y="50.8" smashed="yes">
<attribute name="VALUE" x="83.82" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="167.64" y="50.8" smashed="yes">
<attribute name="VALUE" x="165.1" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="160.02" y="50.8" smashed="yes">
<attribute name="VALUE" x="157.48" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="P+6" gate="VCC" x="152.4" y="119.38" smashed="yes">
<attribute name="VALUE" x="149.86" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="M3" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="VALUE" x="156.21" y="100.33" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="100.33" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M4" gate="G$1" x="149.86" y="81.28" smashed="yes">
<attribute name="VALUE" x="156.21" y="80.01" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="80.01" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC3" gate="PWR" x="45.72" y="147.32" smashed="yes">
<attribute name="NAME" x="43.18" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC4" gate="PWR" x="71.12" y="147.32" smashed="yes">
<attribute name="NAME" x="68.58" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C16" gate="G$1" x="38.1" y="147.32" smashed="yes">
<attribute name="NAME" x="40.64" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="35.56" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="63.5" y="147.32" smashed="yes">
<attribute name="NAME" x="66.04" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="60.96" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="45.72" y="132.08" smashed="yes">
<attribute name="VALUE" x="43.18" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="71.12" y="132.08" smashed="yes">
<attribute name="VALUE" x="68.58" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="71.12" y="162.56" smashed="yes">
<attribute name="VALUE" x="68.58" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+7" gate="VCC" x="45.72" y="162.56" smashed="yes">
<attribute name="VALUE" x="43.18" y="160.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C18" gate="G$1" x="160.02" y="104.14" smashed="yes">
<attribute name="NAME" x="162.56" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="157.48" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+8" gate="VCC" x="208.28" y="96.52" smashed="yes">
<attribute name="VALUE" x="205.74" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND3" gate="1" x="152.4" y="50.8" smashed="yes">
<attribute name="VALUE" x="149.86" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="PGND4" gate="1" x="208.28" y="50.8" smashed="yes">
<attribute name="VALUE" x="205.74" y="48.26" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="P$2"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="88.9" y1="116.84" x2="88.9" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="160.02" y1="109.22" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<pinref part="M3" gate="G$1" pin="D"/>
<pinref part="C18" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IC3" gate="PWR" pin="VDD"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
<wire x1="38.1" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="152.4" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="RAMP"/>
<pinref part="R12" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="TRK"/>
<wire x1="91.44" y1="93.98" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC3" gate="ADP1853" pin="VCCO"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="R11" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="53.34" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="ADP1853" pin="FREQ"/>
<wire x1="91.44" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="142.24" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC3" gate="PWR" pin="VSS"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="137.16"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="137.16" x2="71.12" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC4" gate="PWR" pin="VSS"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<junction x="71.12" y="137.16"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="DH"/>
<wire x1="147.32" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<pinref part="M3" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="BST"/>
<pinref part="C14" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="SW"/>
<wire x1="116.84" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="152.4" y="91.44"/>
<pinref part="C14" gate="G$1" pin="P$1"/>
<wire x1="129.54" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<junction x="134.62" y="91.44"/>
<pinref part="IC3" gate="ADP1853" pin="CS"/>
<wire x1="116.84" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="P$2"/>
<wire x1="129.54" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="134.62" y="88.9"/>
<pinref part="L2" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M3" gate="G$1" pin="S"/>
<pinref part="M4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="P$1"/>
<pinref part="IC3" gate="ADP1853" pin="ILIM"/>
<wire x1="119.38" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="DL"/>
<wire x1="116.84" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="P$2"/>
<wire x1="119.38" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="119.38" y="78.74"/>
<pinref part="M4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="FB"/>
<wire x1="116.84" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="68.58"/>
<pinref part="R9" gate="G$1" pin="P$2"/>
<wire x1="162.56" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="P$1"/>
<pinref part="C10" gate="G$1" pin="P$2"/>
<wire x1="137.16" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="P$1"/>
<pinref part="IC3" gate="ADP1853" pin="COMP"/>
<wire x1="124.46" y1="68.58" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="P$1"/>
<wire x1="121.92" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="121.92" y="68.58"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC4" gate="ACS711" pin="IP+"/>
<wire x1="198.12" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC4" gate="ACS711" pin="VIOUT"/>
<wire x1="175.26" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="SS"/>
<wire x1="91.44" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="152.4" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="157.48" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC4" gate="PWR" pin="VDD"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC4" gate="ACS711" pin="IP-"/>
<wire x1="195.58" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+8" gate="VCC" pin="VBAT"/>
<wire x1="208.28" y1="93.98" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="208.28" y="91.44"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="P$1"/>
<wire x1="132.08" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<junction x="152.4" y="73.66"/>
<pinref part="M4" gate="G$1" pin="S"/>
<pinref part="PGND3" gate="1" pin="PGND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="PGND4" gate="1" pin="PGND"/>
</segment>
</net>
<net name="CC#0" class="0">
<segment>
<pinref part="IC3" gate="ADP1853" pin="ENB"/>
<wire x1="91.44" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">CCLane#1
Delivers 10A to charge the battery</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC7" gate="ADP1853" x="104.14" y="81.28" smashed="yes">
<attribute name="NAME" x="93.98" y="101.6" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+13" gate="VCC" x="88.9" y="119.38" smashed="yes">
<attribute name="VALUE" x="86.36" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="71.12" y="50.8" smashed="yes">
<attribute name="VALUE" x="68.58" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="124.46" y="86.36" smashed="yes">
<attribute name="NAME" x="121.92" y="87.63" size="0.8128" layer="95"/>
<attribute name="VALUE" x="121.92" y="85.09" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R20" gate="G$1" x="127" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="129.54" y="72.39" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="129.54" y="74.93" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="R21" gate="G$1" x="142.24" y="68.58" smashed="yes">
<attribute name="NAME" x="139.7" y="69.85" size="0.8128" layer="95"/>
<attribute name="VALUE" x="139.7" y="67.31" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R22" gate="G$1" x="167.64" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="73.66" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="73.66" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="R23" gate="G$1" x="167.64" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="58.42" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="58.42" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C28" gate="G$1" x="129.54" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="131.445" y="70.485" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="131.445" y="66.675" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C29" gate="G$1" x="132.08" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="133.985" y="66.04" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="133.985" y="60.96" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="IC8" gate="ACS711" x="185.42" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="193.04" y="86.36" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="R24" gate="G$1" x="88.9" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="104.14" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="104.14" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C30" gate="G$1" x="71.12" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="68.58" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="73.66" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C31" gate="G$1" x="78.74" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="81.28" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C32" gate="G$1" x="124.46" y="96.52" smashed="yes" rot="R270">
<attribute name="NAME" x="122.555" y="93.98" size="0.8128" layer="95" align="top-left"/>
<attribute name="VALUE" x="122.555" y="99.06" size="0.8128" layer="95"/>
</instance>
<instance part="C33" gate="G$1" x="208.28" y="83.82" smashed="yes">
<attribute name="NAME" x="210.82" y="81.915" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="205.74" y="81.915" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="L4" gate="G$1" x="180.34" y="91.44" smashed="yes">
<attribute name="NAME" x="177.8" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="177.8" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="GND23" gate="1" x="78.74" y="50.8" smashed="yes">
<attribute name="VALUE" x="76.2" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="86.36" y="50.8" smashed="yes">
<attribute name="VALUE" x="83.82" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="167.64" y="50.8" smashed="yes">
<attribute name="VALUE" x="165.1" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="160.02" y="50.8" smashed="yes">
<attribute name="VALUE" x="157.48" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="P+14" gate="VCC" x="152.4" y="119.38" smashed="yes">
<attribute name="VALUE" x="149.86" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="M7" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="VALUE" x="156.21" y="100.33" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="100.33" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M8" gate="G$1" x="149.86" y="81.28" smashed="yes">
<attribute name="VALUE" x="156.21" y="80.01" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="80.01" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC7" gate="PWR" x="45.72" y="147.32" smashed="yes">
<attribute name="NAME" x="43.18" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC8" gate="PWR" x="71.12" y="147.32" smashed="yes">
<attribute name="NAME" x="68.58" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C34" gate="G$1" x="38.1" y="147.32" smashed="yes">
<attribute name="NAME" x="40.64" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="35.56" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C35" gate="G$1" x="63.5" y="147.32" smashed="yes">
<attribute name="NAME" x="66.04" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="60.96" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND27" gate="1" x="45.72" y="132.08" smashed="yes">
<attribute name="VALUE" x="43.18" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="71.12" y="132.08" smashed="yes">
<attribute name="VALUE" x="68.58" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="71.12" y="162.56" smashed="yes">
<attribute name="VALUE" x="68.58" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+15" gate="VCC" x="45.72" y="162.56" smashed="yes">
<attribute name="VALUE" x="43.18" y="160.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C36" gate="G$1" x="160.02" y="104.14" smashed="yes">
<attribute name="NAME" x="162.56" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="157.48" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+16" gate="VCC" x="208.28" y="96.52" smashed="yes">
<attribute name="VALUE" x="205.74" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND7" gate="1" x="152.4" y="50.8" smashed="yes">
<attribute name="VALUE" x="149.86" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="PGND8" gate="1" x="208.28" y="50.8" smashed="yes">
<attribute name="VALUE" x="205.74" y="48.26" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="P+13" gate="VCC" pin="VCC"/>
<pinref part="R24" gate="G$1" pin="P$2"/>
<wire x1="88.9" y1="116.84" x2="88.9" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="160.02" y1="109.22" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<pinref part="P+14" gate="VCC" pin="VCC"/>
<pinref part="M7" gate="G$1" pin="D"/>
<pinref part="C36" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IC7" gate="PWR" pin="VDD"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
<wire x1="38.1" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="152.4" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+15" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="RAMP"/>
<pinref part="R24" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="TRK"/>
<wire x1="91.44" y1="93.98" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC7" gate="ADP1853" pin="VCCO"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="C31" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="R23" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="53.34" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="C30" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="ADP1853" pin="FREQ"/>
<wire x1="91.44" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="142.24" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC7" gate="PWR" pin="VSS"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="137.16"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="137.16" x2="71.12" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC8" gate="PWR" pin="VSS"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<junction x="71.12" y="137.16"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="DH"/>
<wire x1="147.32" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<pinref part="M7" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="BST"/>
<pinref part="C32" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="SW"/>
<wire x1="116.84" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="152.4" y="91.44"/>
<pinref part="C32" gate="G$1" pin="P$1"/>
<wire x1="129.54" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<junction x="134.62" y="91.44"/>
<pinref part="IC7" gate="ADP1853" pin="CS"/>
<wire x1="116.84" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="P$2"/>
<wire x1="129.54" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="134.62" y="88.9"/>
<pinref part="L4" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M7" gate="G$1" pin="S"/>
<pinref part="M8" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="P$1"/>
<pinref part="IC7" gate="ADP1853" pin="ILIM"/>
<wire x1="119.38" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="DL"/>
<wire x1="116.84" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="P$2"/>
<wire x1="119.38" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="119.38" y="78.74"/>
<pinref part="M8" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="FB"/>
<wire x1="116.84" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="68.58"/>
<pinref part="R21" gate="G$1" pin="P$2"/>
<wire x1="162.56" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
<pinref part="C29" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="P$1"/>
<pinref part="C28" gate="G$1" pin="P$2"/>
<wire x1="137.16" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="C28" gate="G$1" pin="P$1"/>
<pinref part="IC7" gate="ADP1853" pin="COMP"/>
<wire x1="124.46" y1="68.58" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="P$1"/>
<wire x1="121.92" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="121.92" y="68.58"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC8" gate="ACS711" pin="IP+"/>
<wire x1="198.12" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="IC8" gate="ACS711" pin="VIOUT"/>
<wire x1="175.26" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="SS"/>
<wire x1="91.44" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="152.4" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="157.48" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC8" gate="PWR" pin="VDD"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C33" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC8" gate="ACS711" pin="IP-"/>
<wire x1="195.58" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+16" gate="VCC" pin="VBAT"/>
<wire x1="208.28" y1="93.98" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="208.28" y="91.44"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="P$1"/>
<wire x1="132.08" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<junction x="152.4" y="73.66"/>
<pinref part="M8" gate="G$1" pin="S"/>
<pinref part="PGND7" gate="1" pin="PGND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="PGND8" gate="1" pin="PGND"/>
</segment>
</net>
<net name="CC#1" class="0">
<segment>
<pinref part="IC7" gate="ADP1853" pin="ENB"/>
<wire x1="91.44" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">CCLane#2
Delivers 10A to charge the battery</text>
</plain>
<instances>
<instance part="FRAME9" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC12" gate="ADP1853" x="104.14" y="81.28" smashed="yes">
<attribute name="NAME" x="93.98" y="101.6" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+21" gate="VCC" x="88.9" y="119.38" smashed="yes">
<attribute name="VALUE" x="86.36" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND36" gate="1" x="71.12" y="50.8" smashed="yes">
<attribute name="VALUE" x="68.58" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="124.46" y="86.36" smashed="yes">
<attribute name="NAME" x="121.92" y="87.63" size="0.8128" layer="95"/>
<attribute name="VALUE" x="121.92" y="85.09" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R32" gate="G$1" x="127" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="129.54" y="72.39" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="129.54" y="74.93" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="R33" gate="G$1" x="142.24" y="68.58" smashed="yes">
<attribute name="NAME" x="139.7" y="69.85" size="0.8128" layer="95"/>
<attribute name="VALUE" x="139.7" y="67.31" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R37" gate="G$1" x="167.64" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="73.66" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="73.66" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="R38" gate="G$1" x="167.64" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="58.42" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="58.42" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C46" gate="G$1" x="129.54" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="131.445" y="70.485" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="131.445" y="66.675" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C47" gate="G$1" x="132.08" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="133.985" y="66.04" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="133.985" y="60.96" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="IC13" gate="ACS711" x="185.42" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="193.04" y="86.36" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="R39" gate="G$1" x="88.9" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="104.14" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="104.14" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C48" gate="G$1" x="71.12" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="68.58" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="73.66" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C49" gate="G$1" x="78.74" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="81.28" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C51" gate="G$1" x="124.46" y="96.52" smashed="yes" rot="R270">
<attribute name="NAME" x="122.555" y="93.98" size="0.8128" layer="95" align="top-left"/>
<attribute name="VALUE" x="122.555" y="99.06" size="0.8128" layer="95"/>
</instance>
<instance part="C52" gate="G$1" x="208.28" y="83.82" smashed="yes">
<attribute name="NAME" x="210.82" y="81.915" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="205.74" y="81.915" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="L6" gate="G$1" x="180.34" y="91.44" smashed="yes">
<attribute name="NAME" x="177.8" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="177.8" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="GND39" gate="1" x="78.74" y="50.8" smashed="yes">
<attribute name="VALUE" x="76.2" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="86.36" y="50.8" smashed="yes">
<attribute name="VALUE" x="83.82" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND41" gate="1" x="167.64" y="50.8" smashed="yes">
<attribute name="VALUE" x="165.1" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND42" gate="1" x="160.02" y="50.8" smashed="yes">
<attribute name="VALUE" x="157.48" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="P+22" gate="VCC" x="152.4" y="119.38" smashed="yes">
<attribute name="VALUE" x="149.86" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="M11" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="VALUE" x="156.21" y="100.33" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="100.33" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M12" gate="G$1" x="149.86" y="81.28" smashed="yes">
<attribute name="VALUE" x="156.21" y="80.01" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="80.01" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC12" gate="PWR" x="45.72" y="147.32" smashed="yes">
<attribute name="NAME" x="43.18" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC13" gate="PWR" x="71.12" y="147.32" smashed="yes">
<attribute name="NAME" x="68.58" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C53" gate="G$1" x="38.1" y="147.32" smashed="yes">
<attribute name="NAME" x="40.64" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="35.56" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C54" gate="G$1" x="63.5" y="147.32" smashed="yes">
<attribute name="NAME" x="66.04" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="60.96" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND43" gate="1" x="45.72" y="132.08" smashed="yes">
<attribute name="VALUE" x="43.18" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND44" gate="1" x="71.12" y="132.08" smashed="yes">
<attribute name="VALUE" x="68.58" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="71.12" y="162.56" smashed="yes">
<attribute name="VALUE" x="68.58" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+23" gate="VCC" x="45.72" y="162.56" smashed="yes">
<attribute name="VALUE" x="43.18" y="160.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C55" gate="G$1" x="160.02" y="104.14" smashed="yes">
<attribute name="NAME" x="162.56" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="157.48" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+24" gate="VCC" x="208.28" y="96.52" smashed="yes">
<attribute name="VALUE" x="205.74" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND11" gate="1" x="152.4" y="50.8" smashed="yes">
<attribute name="VALUE" x="149.86" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="PGND12" gate="1" x="208.28" y="50.8" smashed="yes">
<attribute name="VALUE" x="205.74" y="48.26" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="P$2"/>
<pinref part="P+21" gate="VCC" pin="VCC"/>
<wire x1="88.9" y1="116.84" x2="88.9" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="160.02" y1="109.22" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<pinref part="P+22" gate="VCC" pin="VCC"/>
<pinref part="M11" gate="G$1" pin="D"/>
<pinref part="C55" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IC12" gate="PWR" pin="VDD"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
<wire x1="38.1" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="152.4" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+23" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="RAMP"/>
<pinref part="R39" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="TRK"/>
<wire x1="91.44" y1="93.98" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC12" gate="ADP1853" pin="VCCO"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND39" gate="1" pin="GND"/>
<pinref part="C49" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND41" gate="1" pin="GND"/>
<pinref part="R38" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="53.34" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="C48" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC12" gate="ADP1853" pin="FREQ"/>
<wire x1="91.44" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="142.24" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC12" gate="PWR" pin="VSS"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="137.16"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="137.16" x2="71.12" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC13" gate="PWR" pin="VSS"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<junction x="71.12" y="137.16"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="DH"/>
<wire x1="147.32" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<pinref part="M11" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="BST"/>
<pinref part="C51" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="SW"/>
<wire x1="116.84" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="152.4" y="91.44"/>
<pinref part="C51" gate="G$1" pin="P$1"/>
<wire x1="129.54" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<junction x="134.62" y="91.44"/>
<pinref part="IC12" gate="ADP1853" pin="CS"/>
<wire x1="116.84" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="P$2"/>
<wire x1="129.54" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="134.62" y="88.9"/>
<pinref part="L6" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M11" gate="G$1" pin="S"/>
<pinref part="M12" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="P$1"/>
<pinref part="IC12" gate="ADP1853" pin="ILIM"/>
<wire x1="119.38" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="DL"/>
<wire x1="116.84" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="P$2"/>
<wire x1="119.38" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="119.38" y="78.74"/>
<pinref part="M12" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="FB"/>
<wire x1="116.84" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="68.58"/>
<pinref part="R33" gate="G$1" pin="P$2"/>
<wire x1="162.56" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
<pinref part="C47" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="P$1"/>
<pinref part="C46" gate="G$1" pin="P$2"/>
<wire x1="137.16" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="C46" gate="G$1" pin="P$1"/>
<pinref part="IC12" gate="ADP1853" pin="COMP"/>
<wire x1="124.46" y1="68.58" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="P$1"/>
<wire x1="121.92" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="121.92" y="68.58"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC13" gate="ACS711" pin="IP+"/>
<wire x1="198.12" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="IC13" gate="ACS711" pin="VIOUT"/>
<wire x1="175.26" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="SS"/>
<wire x1="91.44" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C54" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="152.4" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="157.48" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC13" gate="PWR" pin="VDD"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C52" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC13" gate="ACS711" pin="IP-"/>
<wire x1="195.58" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+24" gate="VCC" pin="VBAT"/>
<wire x1="208.28" y1="93.98" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="208.28" y="91.44"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="P$1"/>
<wire x1="132.08" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<junction x="152.4" y="73.66"/>
<pinref part="M12" gate="G$1" pin="S"/>
<pinref part="PGND11" gate="1" pin="PGND"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="PGND12" gate="1" pin="PGND"/>
</segment>
</net>
<net name="CC#2" class="0">
<segment>
<pinref part="IC12" gate="ADP1853" pin="ENB"/>
<wire x1="91.44" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">CCLane#3
Delivers 10A to charge the battery</text>
</plain>
<instances>
<instance part="FRAME11" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC16" gate="ADP1853" x="104.14" y="81.28" smashed="yes">
<attribute name="NAME" x="93.98" y="101.6" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+31" gate="VCC" x="88.9" y="119.38" smashed="yes">
<attribute name="VALUE" x="86.36" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND52" gate="1" x="71.12" y="50.8" smashed="yes">
<attribute name="VALUE" x="68.58" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="R46" gate="G$1" x="124.46" y="86.36" smashed="yes">
<attribute name="NAME" x="121.92" y="87.63" size="0.8128" layer="95"/>
<attribute name="VALUE" x="121.92" y="85.09" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R47" gate="G$1" x="127" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="129.54" y="72.39" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="129.54" y="74.93" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="R48" gate="G$1" x="142.24" y="68.58" smashed="yes">
<attribute name="NAME" x="139.7" y="69.85" size="0.8128" layer="95"/>
<attribute name="VALUE" x="139.7" y="67.31" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R49" gate="G$1" x="167.64" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="73.66" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="73.66" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="R50" gate="G$1" x="167.64" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="166.37" y="58.42" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.91" y="58.42" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C65" gate="G$1" x="129.54" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="131.445" y="70.485" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="131.445" y="66.675" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C66" gate="G$1" x="132.08" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="133.985" y="66.04" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="133.985" y="60.96" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="IC17" gate="ACS711" x="185.42" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="193.04" y="86.36" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="R51" gate="G$1" x="88.9" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="87.63" y="104.14" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="104.14" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="C67" gate="G$1" x="71.12" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="68.58" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="73.66" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C68" gate="G$1" x="78.74" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="70.485" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="81.28" y="70.485" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C69" gate="G$1" x="124.46" y="96.52" smashed="yes" rot="R270">
<attribute name="NAME" x="122.555" y="93.98" size="0.8128" layer="95" align="top-left"/>
<attribute name="VALUE" x="122.555" y="99.06" size="0.8128" layer="95"/>
</instance>
<instance part="C70" gate="G$1" x="208.28" y="83.82" smashed="yes">
<attribute name="NAME" x="210.82" y="81.915" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="205.74" y="81.915" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="L8" gate="G$1" x="180.34" y="91.44" smashed="yes">
<attribute name="NAME" x="177.8" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="177.8" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="GND53" gate="1" x="78.74" y="50.8" smashed="yes">
<attribute name="VALUE" x="76.2" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND54" gate="1" x="86.36" y="50.8" smashed="yes">
<attribute name="VALUE" x="83.82" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND55" gate="1" x="167.64" y="50.8" smashed="yes">
<attribute name="VALUE" x="165.1" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND56" gate="1" x="160.02" y="50.8" smashed="yes">
<attribute name="VALUE" x="157.48" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="P+32" gate="VCC" x="152.4" y="119.38" smashed="yes">
<attribute name="VALUE" x="149.86" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="M15" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="VALUE" x="156.21" y="100.33" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="100.33" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M16" gate="G$1" x="149.86" y="81.28" smashed="yes">
<attribute name="VALUE" x="156.21" y="80.01" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="148.59" y="80.01" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC16" gate="PWR" x="45.72" y="147.32" smashed="yes">
<attribute name="NAME" x="43.18" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC17" gate="PWR" x="71.12" y="147.32" smashed="yes">
<attribute name="NAME" x="68.58" y="147.32" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C71" gate="G$1" x="38.1" y="147.32" smashed="yes">
<attribute name="NAME" x="40.64" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="35.56" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C72" gate="G$1" x="63.5" y="147.32" smashed="yes">
<attribute name="NAME" x="66.04" y="145.415" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="60.96" y="145.415" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND57" gate="1" x="45.72" y="132.08" smashed="yes">
<attribute name="VALUE" x="43.18" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND58" gate="1" x="71.12" y="132.08" smashed="yes">
<attribute name="VALUE" x="68.58" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="71.12" y="162.56" smashed="yes">
<attribute name="VALUE" x="68.58" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+33" gate="VCC" x="45.72" y="162.56" smashed="yes">
<attribute name="VALUE" x="43.18" y="160.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C73" gate="G$1" x="160.02" y="104.14" smashed="yes">
<attribute name="NAME" x="162.56" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="157.48" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+34" gate="VCC" x="208.28" y="96.52" smashed="yes">
<attribute name="VALUE" x="205.74" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND15" gate="1" x="152.4" y="50.8" smashed="yes">
<attribute name="VALUE" x="149.86" y="48.26" size="1.778" layer="96"/>
</instance>
<instance part="PGND16" gate="1" x="208.28" y="50.8" smashed="yes">
<attribute name="VALUE" x="205.74" y="48.26" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="P$2"/>
<pinref part="P+31" gate="VCC" pin="VCC"/>
<wire x1="88.9" y1="116.84" x2="88.9" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="160.02" y1="109.22" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<pinref part="P+32" gate="VCC" pin="VCC"/>
<pinref part="M15" gate="G$1" pin="D"/>
<pinref part="C73" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IC16" gate="PWR" pin="VDD"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
<wire x1="38.1" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="152.4" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+33" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="RAMP"/>
<pinref part="R51" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="TRK"/>
<wire x1="91.44" y1="93.98" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C68" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC16" gate="ADP1853" pin="VCCO"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<pinref part="C68" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="53.34" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<pinref part="R50" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="53.34" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="C67" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC16" gate="ADP1853" pin="FREQ"/>
<wire x1="91.44" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="P$2"/>
<wire x1="38.1" y1="142.24" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="38.1" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC16" gate="PWR" pin="VSS"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="45.72" y1="137.16" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="137.16"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="137.16" x2="71.12" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC17" gate="PWR" pin="VSS"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="134.62" width="0.1524" layer="91"/>
<junction x="71.12" y="137.16"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="DH"/>
<wire x1="147.32" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<pinref part="M15" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="BST"/>
<pinref part="C69" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="SW"/>
<wire x1="116.84" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="152.4" y="91.44"/>
<pinref part="C69" gate="G$1" pin="P$1"/>
<wire x1="129.54" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<junction x="134.62" y="91.44"/>
<pinref part="IC16" gate="ADP1853" pin="CS"/>
<wire x1="116.84" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="P$2"/>
<wire x1="129.54" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="134.62" y="88.9"/>
<pinref part="L8" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="91.44" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M15" gate="G$1" pin="S"/>
<pinref part="M16" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="P$1"/>
<pinref part="IC16" gate="ADP1853" pin="ILIM"/>
<wire x1="119.38" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="DL"/>
<wire x1="116.84" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="P$2"/>
<wire x1="119.38" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="119.38" y="78.74"/>
<pinref part="M16" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="FB"/>
<wire x1="116.84" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="P$1"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="68.58"/>
<pinref part="R48" gate="G$1" pin="P$2"/>
<wire x1="162.56" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
<pinref part="C66" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="P$1"/>
<pinref part="C65" gate="G$1" pin="P$2"/>
<wire x1="137.16" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="C65" gate="G$1" pin="P$1"/>
<pinref part="IC16" gate="ADP1853" pin="COMP"/>
<wire x1="124.46" y1="68.58" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C66" gate="G$1" pin="P$1"/>
<wire x1="121.92" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="121.92" y="68.58"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC17" gate="ACS711" pin="IP+"/>
<wire x1="198.12" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="IC17" gate="ACS711" pin="VIOUT"/>
<wire x1="175.26" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="SS"/>
<wire x1="91.44" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="P$2"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C72" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="152.4" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="157.48" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC17" gate="PWR" pin="VDD"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C70" gate="G$1" pin="P$1"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC17" gate="ACS711" pin="IP-"/>
<wire x1="195.58" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+34" gate="VCC" pin="VBAT"/>
<wire x1="208.28" y1="93.98" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="208.28" y="91.44"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="P$1"/>
<wire x1="132.08" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<junction x="152.4" y="73.66"/>
<pinref part="M16" gate="G$1" pin="S"/>
<pinref part="PGND15" gate="1" pin="PGND"/>
</segment>
<segment>
<pinref part="C70" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="PGND16" gate="1" pin="PGND"/>
</segment>
</net>
<net name="CC#3" class="0">
<segment>
<pinref part="IC16" gate="ADP1853" pin="ENB"/>
<wire x1="91.44" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">CVLane#0
Delivers 8.4V and up to 50A to 
charge the battery</text>
</plain>
<instances>
<instance part="FRAME13" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC20" gate="ADP1853" x="48.26" y="83.82" smashed="yes">
<attribute name="NAME" x="38.1" y="104.14" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="R60" gate="G$1" x="27.94" y="101.6" smashed="yes">
<attribute name="NAME" x="25.4" y="102.87" size="0.8128" layer="95"/>
<attribute name="VALUE" x="25.4" y="100.33" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R61" gate="G$1" x="71.12" y="88.9" smashed="yes">
<attribute name="NAME" x="68.58" y="90.17" size="0.8128" layer="95"/>
<attribute name="VALUE" x="68.58" y="87.63" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R62" gate="G$1" x="71.12" y="76.2" smashed="yes">
<attribute name="NAME" x="68.58" y="77.47" size="0.8128" layer="95"/>
<attribute name="VALUE" x="68.58" y="74.93" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R63" gate="G$1" x="106.68" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="107.95" y="68.58" size="0.8128" layer="95" rot="R270"/>
<attribute name="VALUE" x="105.41" y="68.58" size="0.8128" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="R64" gate="G$1" x="106.68" y="81.28" smashed="yes" rot="R270">
<attribute name="NAME" x="107.95" y="83.82" size="0.8128" layer="95" rot="R270"/>
<attribute name="VALUE" x="105.41" y="83.82" size="0.8128" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="C87" gate="G$1" x="20.32" y="71.12" smashed="yes">
<attribute name="NAME" x="22.86" y="69.215" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="17.78" y="69.215" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C88" gate="G$1" x="27.94" y="71.12" smashed="yes">
<attribute name="NAME" x="30.48" y="69.215" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="25.4" y="69.215" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C89" gate="G$1" x="91.44" y="104.14" smashed="yes">
<attribute name="NAME" x="93.98" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="88.9" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C90" gate="G$1" x="71.12" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="73.025" y="101.6" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="73.025" y="96.52" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C91" gate="G$1" x="114.3" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="83.185" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="116.84" y="83.185" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C92" gate="G$1" x="121.92" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="119.38" y="83.185" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="124.46" y="83.185" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="L11" gate="G$1" x="99.06" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="101.6" y="90.17" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="101.6" y="92.71" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="M21" gate="G$1" x="81.28" y="83.82" smashed="yes">
<attribute name="VALUE" x="87.63" y="82.55" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="80.01" y="82.55" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M22" gate="G$1" x="81.28" y="104.14" smashed="yes">
<attribute name="VALUE" x="87.63" y="102.87" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="80.01" y="102.87" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="P+43" gate="VCC" x="121.92" y="96.52" smashed="yes">
<attribute name="VALUE" x="119.38" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND23" gate="1" x="83.82" y="53.34" smashed="yes">
<attribute name="VALUE" x="81.28" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="PGND24" gate="1" x="114.3" y="53.34" smashed="yes">
<attribute name="VALUE" x="111.76" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="PGND25" gate="1" x="121.92" y="53.34" smashed="yes">
<attribute name="VALUE" x="119.38" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND67" gate="1" x="27.94" y="53.34" smashed="yes">
<attribute name="VALUE" x="25.4" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND68" gate="1" x="20.32" y="53.34" smashed="yes">
<attribute name="VALUE" x="17.78" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND69" gate="1" x="106.68" y="53.34" smashed="yes">
<attribute name="VALUE" x="104.14" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="91.44" y="53.34" smashed="yes">
<attribute name="VALUE" x="88.9" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="P+44" gate="VCC" x="83.82" y="116.84" smashed="yes">
<attribute name="VALUE" x="81.28" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+45" gate="VCC" x="20.32" y="116.84" smashed="yes">
<attribute name="VALUE" x="17.78" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC21" gate="ADP1853" x="170.18" y="83.82" smashed="yes">
<attribute name="NAME" x="160.02" y="104.14" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="R65" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="NAME" x="147.32" y="102.87" size="0.8128" layer="95"/>
<attribute name="VALUE" x="147.32" y="100.33" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R66" gate="G$1" x="193.04" y="88.9" smashed="yes">
<attribute name="NAME" x="190.5" y="90.17" size="0.8128" layer="95"/>
<attribute name="VALUE" x="190.5" y="87.63" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R67" gate="G$1" x="193.04" y="76.2" smashed="yes">
<attribute name="NAME" x="190.5" y="77.47" size="0.8128" layer="95"/>
<attribute name="VALUE" x="190.5" y="74.93" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="C93" gate="G$1" x="142.24" y="71.12" smashed="yes">
<attribute name="NAME" x="144.78" y="69.215" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="139.7" y="69.215" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C94" gate="G$1" x="213.36" y="104.14" smashed="yes">
<attribute name="NAME" x="215.9" y="102.235" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="210.82" y="102.235" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C95" gate="G$1" x="193.04" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="194.945" y="101.6" size="0.8128" layer="95" rot="R180" align="top-left"/>
<attribute name="VALUE" x="194.945" y="96.52" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="C96" gate="G$1" x="236.22" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="233.68" y="83.185" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="238.76" y="83.185" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="C97" gate="G$1" x="243.84" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="241.3" y="83.185" size="0.8128" layer="95" rot="R270" align="top-left"/>
<attribute name="VALUE" x="246.38" y="83.185" size="0.8128" layer="95" rot="R270"/>
</instance>
<instance part="L12" gate="G$1" x="220.98" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="223.52" y="90.17" size="0.8128" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.52" y="92.71" size="0.8128" layer="95" rot="R180" align="top-left"/>
</instance>
<instance part="M23" gate="G$1" x="203.2" y="83.82" smashed="yes">
<attribute name="VALUE" x="209.55" y="82.55" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="201.93" y="82.55" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="M24" gate="G$1" x="203.2" y="104.14" smashed="yes">
<attribute name="VALUE" x="209.55" y="102.87" size="0.8128" layer="96" rot="MR270" align="bottom-right"/>
<attribute name="NAME" x="201.93" y="102.87" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="P+46" gate="VCC" x="243.84" y="96.52" smashed="yes">
<attribute name="VALUE" x="241.3" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PGND26" gate="1" x="205.74" y="53.34" smashed="yes">
<attribute name="VALUE" x="203.2" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="PGND27" gate="1" x="236.22" y="53.34" smashed="yes">
<attribute name="VALUE" x="233.68" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="PGND28" gate="1" x="243.84" y="53.34" smashed="yes">
<attribute name="VALUE" x="241.3" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND71" gate="1" x="142.24" y="53.34" smashed="yes">
<attribute name="VALUE" x="139.7" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND72" gate="1" x="213.36" y="53.34" smashed="yes">
<attribute name="VALUE" x="210.82" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="P+47" gate="VCC" x="205.74" y="116.84" smashed="yes">
<attribute name="VALUE" x="203.2" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+48" gate="VCC" x="142.24" y="116.84" smashed="yes">
<attribute name="VALUE" x="139.7" y="114.3" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC20" gate="PWR" x="53.34" y="149.86" smashed="yes">
<attribute name="NAME" x="50.8" y="149.86" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC21" gate="PWR" x="78.74" y="149.86" smashed="yes">
<attribute name="NAME" x="76.2" y="149.86" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+49" gate="VCC" x="53.34" y="165.1" smashed="yes">
<attribute name="VALUE" x="50.8" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+50" gate="VCC" x="78.74" y="165.1" smashed="yes">
<attribute name="VALUE" x="76.2" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C98" gate="G$1" x="60.96" y="149.86" smashed="yes">
<attribute name="NAME" x="63.5" y="147.955" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="58.42" y="147.955" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C99" gate="G$1" x="86.36" y="149.86" smashed="yes">
<attribute name="NAME" x="88.9" y="147.955" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="83.82" y="147.955" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="GND73" gate="1" x="53.34" y="134.62" smashed="yes">
<attribute name="VALUE" x="50.8" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="GND74" gate="1" x="78.74" y="134.62" smashed="yes">
<attribute name="VALUE" x="76.2" y="132.08" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
<bus name="SYNC:CLK,COMP,PGOOD,SS">
<segment>
<wire x1="33.02" y1="40.64" x2="187.96" y2="40.64" width="0.762" layer="92"/>
<label x="101.6" y="35.56" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="N$124" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="RAMP"/>
<pinref part="R60" gate="G$1" pin="P$2"/>
<wire x1="35.56" y1="101.6" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="TRK"/>
<wire x1="35.56" y1="96.52" x2="20.32" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C87" gate="G$1" pin="P$1"/>
<wire x1="20.32" y1="96.52" x2="20.32" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC20" gate="ADP1853" pin="VCCO"/>
<wire x1="20.32" y1="93.98" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="35.56" y1="93.98" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="20.32" y="93.98"/>
<pinref part="IC20" gate="ADP1853" pin="SYNC"/>
<wire x1="27.94" y1="93.98" x2="20.32" y2="93.98" width="0.1524" layer="91"/>
<wire x1="35.56" y1="86.36" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="27.94" y="93.98"/>
<pinref part="IC20" gate="ADP1853" pin="FREQ"/>
<wire x1="35.56" y1="81.28" x2="27.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="27.94" y1="81.28" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<junction x="27.94" y="86.36"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C88" gate="G$1" pin="P$2"/>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND68" gate="1" pin="GND"/>
<pinref part="C87" gate="G$1" pin="P$2"/>
<wire x1="20.32" y1="55.88" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND69" gate="1" pin="GND"/>
<pinref part="R63" gate="G$1" pin="P$2"/>
<wire x1="106.68" y1="55.88" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND70" gate="1" pin="GND"/>
<pinref part="C89" gate="G$1" pin="P$2"/>
<wire x1="91.44" y1="55.88" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND71" gate="1" pin="GND"/>
<pinref part="C93" gate="G$1" pin="P$2"/>
<wire x1="142.24" y1="55.88" x2="142.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND72" gate="1" pin="GND"/>
<pinref part="C94" gate="G$1" pin="P$2"/>
<wire x1="213.36" y1="55.88" x2="213.36" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND73" gate="1" pin="GND"/>
<pinref part="IC20" gate="PWR" pin="VSS"/>
<wire x1="53.34" y1="137.16" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C98" gate="G$1" pin="P$2"/>
<wire x1="53.34" y1="139.7" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<wire x1="60.96" y1="144.78" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="60.96" y1="139.7" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<junction x="53.34" y="139.7"/>
</segment>
<segment>
<pinref part="C99" gate="G$1" pin="P$2"/>
<wire x1="86.36" y1="144.78" x2="86.36" y2="139.7" width="0.1524" layer="91"/>
<wire x1="86.36" y1="139.7" x2="78.74" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND74" gate="1" pin="GND"/>
<wire x1="78.74" y1="139.7" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC21" gate="PWR" pin="VSS"/>
<wire x1="78.74" y1="142.24" x2="78.74" y2="139.7" width="0.1524" layer="91"/>
<junction x="78.74" y="139.7"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="DL"/>
<pinref part="M21" gate="G$1" pin="G"/>
<wire x1="60.96" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="81.28" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<wire x1="66.04" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<junction x="63.5" y="81.28"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="ILIM"/>
<pinref part="R61" gate="G$1" pin="P$1"/>
<wire x1="60.96" y1="88.9" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="DH"/>
<pinref part="M22" gate="G$1" pin="G"/>
<wire x1="60.96" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<pinref part="C90" gate="G$1" pin="P$1"/>
<pinref part="IC20" gate="ADP1853" pin="BST"/>
<wire x1="66.04" y1="99.06" x2="60.96" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<pinref part="C90" gate="G$1" pin="P$2"/>
<wire x1="76.2" y1="99.06" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<wire x1="78.74" y1="99.06" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC20" gate="ADP1853" pin="SW"/>
<wire x1="78.74" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC20" gate="ADP1853" pin="CS"/>
<wire x1="60.96" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<junction x="78.74" y="93.98"/>
<pinref part="R61" gate="G$1" pin="P$2"/>
<wire x1="76.2" y1="88.9" x2="78.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="88.9" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
<wire x1="78.74" y1="93.98" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<pinref part="M21" gate="G$1" pin="D"/>
<wire x1="83.82" y1="93.98" x2="83.82" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M22" gate="G$1" pin="S"/>
<wire x1="83.82" y1="91.44" x2="83.82" y2="88.9" width="0.1524" layer="91"/>
<wire x1="83.82" y1="93.98" x2="83.82" y2="99.06" width="0.1524" layer="91"/>
<junction x="83.82" y="93.98"/>
<pinref part="L11" gate="G$1" pin="P$2"/>
<wire x1="93.98" y1="91.44" x2="83.82" y2="91.44" width="0.1524" layer="91"/>
<junction x="83.82" y="91.44"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="PGND23" gate="1" pin="PGND"/>
<pinref part="M21" gate="G$1" pin="S"/>
<wire x1="83.82" y1="78.74" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="P$2"/>
<wire x1="83.82" y1="76.2" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="76.2" y1="76.2" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<junction x="83.82" y="76.2"/>
</segment>
<segment>
<pinref part="PGND24" gate="1" pin="PGND"/>
<pinref part="C91" gate="G$1" pin="P$1"/>
<wire x1="114.3" y1="55.88" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C92" gate="G$1" pin="P$1"/>
<pinref part="PGND25" gate="1" pin="PGND"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PGND26" gate="1" pin="PGND"/>
<pinref part="M23" gate="G$1" pin="S"/>
<wire x1="205.74" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R67" gate="G$1" pin="P$2"/>
<wire x1="205.74" y1="76.2" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="76.2" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="205.74" y="76.2"/>
</segment>
<segment>
<pinref part="PGND27" gate="1" pin="PGND"/>
<pinref part="C96" gate="G$1" pin="P$1"/>
<wire x1="236.22" y1="55.88" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C97" gate="G$1" pin="P$1"/>
<pinref part="PGND28" gate="1" pin="PGND"/>
<wire x1="243.84" y1="76.2" x2="243.84" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="FB"/>
<wire x1="60.96" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="P$1"/>
<wire x1="106.68" y1="73.66" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="P$2"/>
<wire x1="106.68" y1="76.2" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<junction x="106.68" y="73.66"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="C92" gate="G$1" pin="P$2"/>
<pinref part="P+43" gate="VCC" pin="VBAT"/>
<wire x1="121.92" y1="86.36" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C91" gate="G$1" pin="P$2"/>
<wire x1="121.92" y1="91.44" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="114.3" y1="86.36" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<junction x="121.92" y="91.44"/>
<pinref part="L11" gate="G$1" pin="P$1"/>
<wire x1="114.3" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<junction x="114.3" y="91.44"/>
<pinref part="R64" gate="G$1" pin="P$1"/>
<wire x1="106.68" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="106.68" y1="86.36" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<junction x="106.68" y="91.44"/>
</segment>
<segment>
<pinref part="C97" gate="G$1" pin="P$2"/>
<pinref part="P+46" gate="VCC" pin="VBAT"/>
<wire x1="243.84" y1="86.36" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C96" gate="G$1" pin="P$2"/>
<wire x1="243.84" y1="91.44" x2="243.84" y2="93.98" width="0.1524" layer="91"/>
<wire x1="236.22" y1="86.36" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="236.22" y1="91.44" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<junction x="243.84" y="91.44"/>
<pinref part="L12" gate="G$1" pin="P$1"/>
<wire x1="236.22" y1="91.44" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<junction x="236.22" y="91.44"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="C89" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="109.22" x2="91.44" y2="111.76" width="0.1524" layer="91"/>
<wire x1="91.44" y1="111.76" x2="83.82" y2="111.76" width="0.1524" layer="91"/>
<pinref part="M22" gate="G$1" pin="D"/>
<wire x1="83.82" y1="111.76" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
<wire x1="83.82" y1="111.76" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<junction x="83.82" y="111.76"/>
<pinref part="P+44" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="P$1"/>
<pinref part="P+45" gate="VCC" pin="VCC"/>
<wire x1="22.86" y1="101.6" x2="20.32" y2="101.6" width="0.1524" layer="91"/>
<wire x1="20.32" y1="101.6" x2="20.32" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C94" gate="G$1" pin="P$1"/>
<wire x1="213.36" y1="109.22" x2="213.36" y2="111.76" width="0.1524" layer="91"/>
<wire x1="213.36" y1="111.76" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<pinref part="M24" gate="G$1" pin="D"/>
<wire x1="205.74" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="205.74" y1="111.76" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<junction x="205.74" y="111.76"/>
<pinref part="P+47" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="P$1"/>
<pinref part="P+48" gate="VCC" pin="VCC"/>
<wire x1="144.78" y1="101.6" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="142.24" y1="101.6" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C98" gate="G$1" pin="P$1"/>
<wire x1="60.96" y1="154.94" x2="60.96" y2="160.02" width="0.1524" layer="91"/>
<wire x1="60.96" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<pinref part="IC20" gate="PWR" pin="VDD"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+49" gate="VCC" pin="VCC"/>
<wire x1="53.34" y1="162.56" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<junction x="53.34" y="160.02"/>
</segment>
<segment>
<pinref part="IC21" gate="PWR" pin="VDD"/>
<pinref part="P+50" gate="VCC" pin="VCC"/>
<wire x1="78.74" y1="157.48" x2="78.74" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C99" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="160.02" x2="78.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="86.36" y1="154.94" x2="86.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="86.36" y1="160.02" x2="78.74" y2="160.02" width="0.1524" layer="91"/>
<junction x="78.74" y="160.02"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="IC21" gate="ADP1853" pin="RAMP"/>
<pinref part="R65" gate="G$1" pin="P$2"/>
<wire x1="157.48" y1="101.6" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="IC21" gate="ADP1853" pin="TRK"/>
<wire x1="157.48" y1="96.52" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C93" gate="G$1" pin="P$1"/>
<wire x1="142.24" y1="96.52" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC21" gate="ADP1853" pin="VCCO"/>
<wire x1="142.24" y1="93.98" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="157.48" y1="93.98" x2="149.86" y2="93.98" width="0.1524" layer="91"/>
<junction x="142.24" y="93.98"/>
<wire x1="149.86" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="149.86" y1="81.28" x2="149.86" y2="93.98" width="0.1524" layer="91"/>
<junction x="149.86" y="93.98"/>
<pinref part="IC21" gate="ADP1853" pin="FREQ"/>
<wire x1="157.48" y1="81.28" x2="149.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="IC21" gate="ADP1853" pin="FB"/>
<wire x1="182.88" y1="73.66" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="58.42" x2="137.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="137.16" y1="58.42" x2="137.16" y2="93.98" width="0.1524" layer="91"/>
<wire x1="137.16" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<pinref part="IC21" gate="ADP1853" pin="DL"/>
<pinref part="M23" gate="G$1" pin="G"/>
<wire x1="182.88" y1="81.28" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R67" gate="G$1" pin="P$1"/>
<wire x1="185.42" y1="81.28" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="187.96" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<junction x="185.42" y="81.28"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="IC21" gate="ADP1853" pin="ILIM"/>
<pinref part="R66" gate="G$1" pin="P$1"/>
<wire x1="182.88" y1="88.9" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<pinref part="IC21" gate="ADP1853" pin="DH"/>
<pinref part="M24" gate="G$1" pin="G"/>
<wire x1="182.88" y1="101.6" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="C95" gate="G$1" pin="P$1"/>
<pinref part="IC21" gate="ADP1853" pin="BST"/>
<wire x1="187.96" y1="99.06" x2="182.88" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<pinref part="C95" gate="G$1" pin="P$2"/>
<wire x1="198.12" y1="99.06" x2="200.66" y2="99.06" width="0.1524" layer="91"/>
<wire x1="200.66" y1="99.06" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC21" gate="ADP1853" pin="SW"/>
<wire x1="200.66" y1="93.98" x2="182.88" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC21" gate="ADP1853" pin="CS"/>
<wire x1="182.88" y1="91.44" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="200.66" y="93.98"/>
<pinref part="R66" gate="G$1" pin="P$2"/>
<wire x1="198.12" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<junction x="200.66" y="91.44"/>
<wire x1="200.66" y1="93.98" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="M23" gate="G$1" pin="D"/>
<wire x1="205.74" y1="93.98" x2="205.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="M24" gate="G$1" pin="S"/>
<wire x1="205.74" y1="91.44" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="205.74" y1="93.98" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="205.74" y="93.98"/>
<pinref part="L12" gate="G$1" pin="P$2"/>
<wire x1="215.9" y1="91.44" x2="205.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="205.74" y="91.44"/>
</segment>
</net>
<net name="SS" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="SS"/>
<wire x1="35.56" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="78.74" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="27.94" y1="78.74" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
<wire x1="33.02" y1="40.64" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<junction x="33.02" y="78.74"/>
</segment>
<segment>
<pinref part="IC21" gate="ADP1853" pin="SS"/>
<wire x1="157.48" y1="78.74" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
<wire x1="149.86" y1="40.64" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="CLKOUT"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="60.96" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="ADP1853" pin="SYNC"/>
<wire x1="157.48" y1="86.36" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="154.94" y1="86.36" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PGOOD" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="PGOOD"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="ADP1853" pin="PGOOD"/>
<wire x1="182.88" y1="66.04" x2="185.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="185.42" y1="66.04" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="COMP"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="ADP1853" pin="COMP"/>
<wire x1="182.88" y1="71.12" x2="187.96" y2="71.12" width="0.1524" layer="91"/>
<wire x1="187.96" y1="71.12" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CV#0" class="0">
<segment>
<pinref part="IC20" gate="ADP1853" pin="ENB"/>
<wire x1="35.56" y1="99.06" x2="20.32" y2="99.06" width="0.1524" layer="91"/>
<label x="20.32" y="99.06" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC21" gate="ADP1853" pin="ENB"/>
<wire x1="157.48" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<label x="142.24" y="99.06" size="1.778" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">Reversvoltage-,
Overvoltageprotection,
and Balancing
</text>
<wire x1="35.56" y1="106.68" x2="35.56" y2="55.88" width="0.254" layer="98" style="shortdash"/>
<wire x1="35.56" y1="55.88" x2="114.3" y2="55.88" width="0.254" layer="98" style="shortdash"/>
<wire x1="114.3" y1="55.88" x2="114.3" y2="106.68" width="0.254" layer="98" style="shortdash"/>
<wire x1="114.3" y1="106.68" x2="35.56" y2="106.68" width="0.254" layer="98" style="shortdash"/>
<text x="81.28" y="68.58" size="1.778" layer="98" font="vector" align="top-left">Reverse voltage protection</text>
<wire x1="134.62" y1="106.68" x2="134.62" y2="55.88" width="0.254" layer="98" style="shortdash"/>
<wire x1="134.62" y1="55.88" x2="215.9" y2="55.88" width="0.254" layer="98" style="shortdash"/>
<wire x1="215.9" y1="55.88" x2="215.9" y2="106.68" width="0.254" layer="98" style="shortdash"/>
<wire x1="215.9" y1="106.68" x2="134.62" y2="106.68" width="0.254" layer="98" style="shortdash"/>
<text x="149.86" y="104.14" size="1.778" layer="98" font="vector" align="top-left">Cell balancing &amp; overvoltage protection</text>
</plain>
<instances>
<instance part="FRAME7" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC11" gate="BQ29200" x="182.88" y="86.36" smashed="yes">
<attribute name="NAME" x="172.72" y="93.98" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="R34" gate="G$1" x="160.02" y="91.44" smashed="yes">
<attribute name="NAME" x="157.48" y="92.71" size="0.8128" layer="95"/>
<attribute name="VALUE" x="157.48" y="90.17" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R35" gate="G$1" x="160.02" y="88.9" smashed="yes">
<attribute name="NAME" x="157.48" y="90.17" size="0.8128" layer="95"/>
<attribute name="VALUE" x="157.48" y="87.63" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="R36" gate="G$1" x="203.2" y="81.28" smashed="yes">
<attribute name="NAME" x="200.66" y="82.55" size="0.8128" layer="95"/>
<attribute name="VALUE" x="200.66" y="80.01" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="GND37" gate="1" x="167.64" y="63.5" smashed="yes">
<attribute name="VALUE" x="165.1" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="160.02" y="63.5" smashed="yes">
<attribute name="VALUE" x="157.48" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="C50" gate="G$1" x="167.64" y="73.66" smashed="yes">
<attribute name="NAME" x="170.18" y="71.755" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="165.1" y="71.755" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+25" gate="VCC" x="147.32" y="96.52" smashed="yes">
<attribute name="VALUE" x="144.78" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+26" gate="G$1" x="139.7" y="96.52" smashed="yes">
<attribute name="VALUE" x="137.16" y="93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="M1" gate="G$1" x="73.66" y="93.98" smashed="yes" rot="R90">
<attribute name="VALUE" x="74.93" y="100.33" size="0.8128" layer="96" rot="MR180" align="bottom-right"/>
<attribute name="NAME" x="74.93" y="92.71" size="0.8128" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="76.2" y="76.2" smashed="yes" rot="R270">
<attribute name="NAME" x="77.47" y="78.74" size="0.8128" layer="95" rot="R270"/>
<attribute name="VALUE" x="74.93" y="78.74" size="0.8128" layer="95" rot="R270" align="top-left"/>
</instance>
<instance part="P+1" gate="VCC" x="81.28" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="83.82" y="99.06" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND1" gate="1" x="76.2" y="63.5" smashed="yes">
<attribute name="VALUE" x="73.66" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="IC11" gate="PWR" x="55.88" y="149.86" smashed="yes">
<attribute name="NAME" x="53.34" y="149.86" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C1" gate="G$1" x="63.5" y="149.86" smashed="yes">
<attribute name="NAME" x="66.04" y="147.955" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="60.96" y="147.955" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="P+2" gate="VCC" x="55.88" y="165.1" smashed="yes">
<attribute name="VALUE" x="53.34" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="55.88" y="134.62" smashed="yes">
<attribute name="VALUE" x="53.34" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="VIN" gate="G$1" x="43.18" y="88.9" smashed="yes">
<attribute name="NAME" x="39.37" y="99.06" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="PGND13" gate="1" x="63.5" y="63.5" smashed="yes">
<attribute name="VALUE" x="60.96" y="60.96" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$68" class="0">
<segment>
<pinref part="IC11" gate="BQ29200" pin="VC2"/>
<pinref part="R34" gate="G$1" pin="P$2"/>
<wire x1="170.18" y1="91.44" x2="165.1" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="IC11" gate="BQ29200" pin="VC1"/>
<pinref part="R35" gate="G$1" pin="P$2"/>
<wire x1="170.18" y1="88.9" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC11" gate="BQ29200" pin="!CB_EN"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="170.18" y1="83.82" x2="160.02" y2="83.82" width="0.1524" layer="91"/>
<wire x1="160.02" y1="83.82" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="C50" gate="G$1" pin="P$2"/>
<wire x1="167.64" y1="66.04" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="76.2" y1="66.04" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC11" gate="PWR" pin="VSS"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="55.88" y1="142.24" x2="55.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="139.7" x2="55.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="55.88" y1="139.7" x2="63.5" y2="139.7" width="0.1524" layer="91"/>
<junction x="55.88" y="139.7"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="139.7" x2="63.5" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="C50" gate="G$1" pin="P$1"/>
<pinref part="IC11" gate="BQ29200" pin="CD"/>
<wire x1="167.64" y1="78.74" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="167.64" y1="81.28" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="P$1"/>
<pinref part="P+25" gate="VCC" pin="VBAT"/>
<wire x1="154.94" y1="91.44" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="91.44" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="P$1"/>
<wire x1="63.5" y1="154.94" x2="63.5" y2="160.02" width="0.1524" layer="91"/>
<wire x1="63.5" y1="160.02" x2="55.88" y2="160.02" width="0.1524" layer="91"/>
<pinref part="IC11" gate="PWR" pin="VDD"/>
<wire x1="55.88" y1="160.02" x2="55.88" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VBAT"/>
<wire x1="55.88" y1="160.02" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<junction x="55.88" y="160.02"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="P$1"/>
<pinref part="IC11" gate="BQ29200" pin="VC1_CB"/>
<wire x1="198.12" y1="81.28" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBATS2" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="P$2"/>
<wire x1="208.28" y1="81.28" x2="210.82" y2="81.28" width="0.1524" layer="91"/>
<wire x1="210.82" y1="81.28" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<wire x1="210.82" y1="96.52" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<wire x1="152.4" y1="96.52" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="P$1"/>
<wire x1="152.4" y1="88.9" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="152.4" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<junction x="152.4" y="88.9"/>
<pinref part="P+26" gate="G$1" pin="VBATS2"/>
<wire x1="139.7" y1="88.9" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="81.28" y1="96.52" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="S"/>
<wire x1="78.74" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<pinref part="M1" gate="G$1" pin="G"/>
<wire x1="76.2" y1="81.28" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="D"/>
<pinref part="VIN" gate="G$1" pin="1"/>
<wire x1="48.26" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAL" class="0">
<segment>
<pinref part="IC11" gate="BQ29200" pin="OUT"/>
<wire x1="195.58" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<label x="198.12" y="91.44" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="BAT_COM1" class="0">
<segment>
<pinref part="VIN" gate="G$1" pin="2"/>
<wire x1="48.26" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<label x="53.34" y="91.44" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="BAT_COM2" class="0">
<segment>
<pinref part="VIN" gate="G$1" pin="3"/>
<wire x1="48.26" y1="86.36" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<label x="53.34" y="86.36" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="VIN" gate="G$1" pin="4"/>
<wire x1="48.26" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<pinref part="PGND13" gate="1" pin="PGND"/>
<wire x1="63.5" y1="66.04" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="22.86" size="1.778" layer="94" font="vector" align="top-left">Powerdistribution</text>
</plain>
<instances>
<instance part="FRAME8" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="205.74" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="205.74" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="219.075" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="PGND2" gate="1" x="177.8" y="142.24" smashed="yes">
<attribute name="VALUE" x="175.26" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="PGND5" gate="1" x="177.8" y="114.3" smashed="yes">
<attribute name="VALUE" x="175.26" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="PGND6" gate="1" x="177.8" y="86.36" smashed="yes">
<attribute name="VALUE" x="175.26" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="PGND9" gate="1" x="177.8" y="58.42" smashed="yes">
<attribute name="VALUE" x="175.26" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="PGND10" gate="1" x="177.8" y="30.48" smashed="yes">
<attribute name="VALUE" x="175.26" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="PGND1" gate="1" x="33.02" y="101.6" smashed="yes">
<attribute name="VALUE" x="30.48" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="P+19" gate="VCC" x="33.02" y="139.7" smashed="yes">
<attribute name="VALUE" x="30.48" y="137.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="170.18" y="154.94" smashed="yes">
<attribute name="NAME" x="172.72" y="153.035" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="167.64" y="153.035" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C19" gate="G$1" x="170.18" y="127" smashed="yes">
<attribute name="NAME" x="172.72" y="125.095" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="167.64" y="125.095" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C20" gate="G$1" x="170.18" y="99.06" smashed="yes">
<attribute name="NAME" x="172.72" y="97.155" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="167.64" y="97.155" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C21" gate="G$1" x="170.18" y="71.12" smashed="yes">
<attribute name="NAME" x="172.72" y="69.215" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="167.64" y="69.215" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="C22" gate="G$1" x="170.18" y="43.18" smashed="yes">
<attribute name="NAME" x="172.72" y="41.275" size="0.8128" layer="95" rot="R90" align="top-left"/>
<attribute name="VALUE" x="167.64" y="41.275" size="0.8128" layer="95" rot="R90"/>
</instance>
<instance part="M2" gate="G$1" x="48.26" y="132.08" smashed="yes" rot="MR90">
<attribute name="VALUE" x="46.99" y="138.43" size="0.8128" layer="96" rot="R180" align="bottom-right"/>
<attribute name="NAME" x="46.99" y="130.81" size="0.8128" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="R3" gate="G$1" x="40.64" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="39.37" y="124.46" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.91" y="124.46" size="0.8128" layer="95" rot="R90" align="top-left"/>
</instance>
<instance part="Q1" gate="G$1" x="48.26" y="111.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="50.8" y="114.3" size="0.8128" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="43.18" y="114.3" size="0.8128" layer="96" font="vector" rot="MR90"/>
</instance>
<instance part="GND6" gate="1" x="45.72" y="101.6" smashed="yes">
<attribute name="VALUE" x="43.18" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="P+18" gate="G$1" x="58.42" y="139.7" smashed="yes">
<attribute name="VALUE" x="55.88" y="137.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="58.42" y="111.76" smashed="yes">
<attribute name="NAME" x="55.88" y="113.03" size="0.8128" layer="95"/>
<attribute name="VALUE" x="55.88" y="110.49" size="0.8128" layer="95" align="top-left"/>
</instance>
<instance part="P+9" gate="G$1" x="177.8" y="55.88" smashed="yes">
<attribute name="VALUE" x="175.26" y="53.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+10" gate="G$1" x="177.8" y="83.82" smashed="yes">
<attribute name="VALUE" x="175.26" y="81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+11" gate="G$1" x="177.8" y="111.76" smashed="yes">
<attribute name="VALUE" x="175.26" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+12" gate="G$1" x="177.8" y="139.7" smashed="yes">
<attribute name="VALUE" x="175.26" y="137.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+17" gate="G$1" x="177.8" y="167.64" smashed="yes">
<attribute name="VALUE" x="175.26" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="BAT" gate="G$1" x="25.4" y="129.54" smashed="yes">
<attribute name="NAME" x="21.59" y="137.16" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="P+3" gate="G$1" x="38.1" y="139.7" smashed="yes">
<attribute name="VALUE" x="35.56" y="137.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="FOOT_R" gate="G$1" x="195.58" y="43.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="199.39" y="53.34" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="FOOT_L" gate="G$1" x="195.58" y="71.12" smashed="yes" rot="MR0">
<attribute name="NAME" x="199.39" y="81.28" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="ARM_R" gate="G$1" x="195.58" y="99.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="199.39" y="109.22" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="ARM_L" gate="G$1" x="195.58" y="127" smashed="yes" rot="MR0">
<attribute name="NAME" x="199.39" y="137.16" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="HEAD" gate="G$1" x="195.58" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="199.39" y="165.1" size="0.8128" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="GND" gate="G$1" x="25.4" y="71.12" smashed="yes">
<attribute name="NAME" x="21.59" y="76.2" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="PGND14" gate="1" x="33.02" y="58.42" smashed="yes">
<attribute name="VALUE" x="30.48" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="40.64" y="58.42" smashed="yes">
<attribute name="VALUE" x="38.1" y="55.88" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
<bus name="CAN:CAN_H,CAN_L">
<segment>
<wire x1="182.88" y1="162.56" x2="182.88" y2="35.56" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="CAN_H" class="0">
<segment>
<wire x1="182.88" y1="162.56" x2="190.5" y2="162.56" width="0.1524" layer="91"/>
<pinref part="HEAD" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="182.88" y1="134.62" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<pinref part="ARM_L" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="182.88" y1="106.68" x2="190.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="ARM_R" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="182.88" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<pinref part="FOOT_L" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="182.88" y1="50.8" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="FOOT_R" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CAN_L" class="0">
<segment>
<wire x1="182.88" y1="147.32" x2="190.5" y2="147.32" width="0.1524" layer="91"/>
<pinref part="HEAD" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="182.88" y1="119.38" x2="190.5" y2="119.38" width="0.1524" layer="91"/>
<pinref part="ARM_L" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="182.88" y1="91.44" x2="190.5" y2="91.44" width="0.1524" layer="91"/>
<pinref part="ARM_R" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="182.88" y1="63.5" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<pinref part="FOOT_L" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="182.88" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="FOOT_R" gate="G$1" pin="4"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="PGND2" gate="1" pin="PGND"/>
<wire x1="190.5" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="177.8" y1="152.4" x2="177.8" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="147.32" x2="177.8" y2="144.78" width="0.1524" layer="91"/>
<wire x1="170.18" y1="149.86" x2="170.18" y2="147.32" width="0.1524" layer="91"/>
<wire x1="170.18" y1="147.32" x2="177.8" y2="147.32" width="0.1524" layer="91"/>
<junction x="177.8" y="147.32"/>
<pinref part="HEAD" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="PGND5" gate="1" pin="PGND"/>
<wire x1="190.5" y1="124.46" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<wire x1="177.8" y1="124.46" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="119.38" x2="177.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="170.18" y1="121.92" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<wire x1="170.18" y1="119.38" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
<junction x="177.8" y="119.38"/>
<pinref part="ARM_L" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="PGND6" gate="1" pin="PGND"/>
<wire x1="190.5" y1="96.52" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<wire x1="177.8" y1="96.52" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="91.44" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="170.18" y1="93.98" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="91.44" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="177.8" y="91.44"/>
<pinref part="ARM_R" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="PGND9" gate="1" pin="PGND"/>
<wire x1="190.5" y1="68.58" x2="177.8" y2="68.58" width="0.1524" layer="91"/>
<wire x1="177.8" y1="68.58" x2="177.8" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="63.5" x2="177.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="170.18" y1="66.04" x2="170.18" y2="63.5" width="0.1524" layer="91"/>
<wire x1="170.18" y1="63.5" x2="177.8" y2="63.5" width="0.1524" layer="91"/>
<junction x="177.8" y="63.5"/>
<pinref part="FOOT_L" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="PGND10" gate="1" pin="PGND"/>
<wire x1="190.5" y1="40.64" x2="177.8" y2="40.64" width="0.1524" layer="91"/>
<wire x1="177.8" y1="40.64" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="P$2"/>
<wire x1="177.8" y1="35.56" x2="177.8" y2="33.02" width="0.1524" layer="91"/>
<wire x1="170.18" y1="38.1" x2="170.18" y2="35.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="35.56" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="177.8" y="35.56"/>
<pinref part="FOOT_R" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="PGND1" gate="1" pin="PGND"/>
<wire x1="33.02" y1="104.14" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="124.46" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<pinref part="BAT" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="GND" gate="G$1" pin="2"/>
<pinref part="PGND14" gate="1" pin="PGND"/>
<wire x1="30.48" y1="68.58" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="68.58" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<wire x1="30.48" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<pinref part="P+19" gate="VCC" pin="VBAT"/>
<wire x1="33.02" y1="134.62" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="134.62" width="0.1524" layer="91"/>
<pinref part="M2" gate="G$1" pin="S"/>
<wire x1="40.64" y1="134.62" x2="43.18" y2="134.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<junction x="40.64" y="134.62"/>
<junction x="33.02" y="134.62"/>
<pinref part="BAT" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="M2" gate="G$1" pin="G"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="40.64" y1="121.92" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="40.64" y1="119.38" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<wire x1="45.72" y1="119.38" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="C"/>
<wire x1="45.72" y1="116.84" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<junction x="45.72" y="119.38"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="Q1" gate="G$1" pin="E"/>
<wire x1="45.72" y1="104.14" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="GND" gate="G$1" pin="1"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="73.66" x2="30.48" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="M2" gate="G$1" pin="D"/>
<wire x1="53.34" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<pinref part="P+18" gate="G$1" pin="VOUT"/>
<wire x1="58.42" y1="134.62" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="190.5" y1="45.72" x2="177.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="177.8" y1="45.72" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="50.8" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="170.18" y1="48.26" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="170.18" y1="50.8" x2="177.8" y2="50.8" width="0.1524" layer="91"/>
<junction x="177.8" y="50.8"/>
<pinref part="P+9" gate="G$1" pin="VOUT"/>
<pinref part="FOOT_R" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="190.5" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="177.8" y1="73.66" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="78.74" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="76.2" x2="170.18" y2="78.74" width="0.1524" layer="91"/>
<wire x1="170.18" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<junction x="177.8" y="78.74"/>
<pinref part="P+10" gate="G$1" pin="VOUT"/>
<pinref part="FOOT_L" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="190.5" y1="101.6" x2="177.8" y2="101.6" width="0.1524" layer="91"/>
<wire x1="177.8" y1="101.6" x2="177.8" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="106.68" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="170.18" y1="104.14" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="170.18" y1="106.68" x2="177.8" y2="106.68" width="0.1524" layer="91"/>
<junction x="177.8" y="106.68"/>
<pinref part="P+11" gate="G$1" pin="VOUT"/>
<pinref part="ARM_R" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="190.5" y1="129.54" x2="177.8" y2="129.54" width="0.1524" layer="91"/>
<wire x1="177.8" y1="129.54" x2="177.8" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="134.62" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="170.18" y1="132.08" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
<wire x1="170.18" y1="134.62" x2="177.8" y2="134.62" width="0.1524" layer="91"/>
<junction x="177.8" y="134.62"/>
<pinref part="P+12" gate="G$1" pin="VOUT"/>
<pinref part="ARM_L" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="190.5" y1="157.48" x2="177.8" y2="157.48" width="0.1524" layer="91"/>
<wire x1="177.8" y1="157.48" x2="177.8" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$1"/>
<wire x1="177.8" y1="162.56" x2="177.8" y2="165.1" width="0.1524" layer="91"/>
<wire x1="170.18" y1="160.02" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="162.56" x2="177.8" y2="162.56" width="0.1524" layer="91"/>
<junction x="177.8" y="162.56"/>
<pinref part="P+17" gate="G$1" pin="VOUT"/>
<pinref part="HEAD" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="B"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="53.34" y1="111.76" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT_CTRL" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="P$2"/>
<wire x1="63.5" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<label x="66.04" y="111.76" size="1.778" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="VBATS2" class="0">
<segment>
<pinref part="BAT" gate="G$1" pin="2"/>
<wire x1="30.48" y1="129.54" x2="38.1" y2="129.54" width="0.1524" layer="91"/>
<wire x1="38.1" y1="129.54" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<pinref part="P+3" gate="G$1" pin="VBATS2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
