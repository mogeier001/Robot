// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 05.03.2020
// Project Name	: SmartServoV2
// File Name	: Filter
// Version		: 1
// Description	: This is a simple low
//				  first order low pass
//				  filter.
//
// ------------------------------------ //

template<class T>
Filter<T>::Filter(double alpha)
{
	SetAlpha(alpha);
	_lastResult = 0;
}

template<class T>
T Filter<T>::Calculate(T value)
{
	return _lastResult = (T)(_alpha * _lastResult +(1-_alpha)*value);
}

template<class T>
void Filter<T>::SetAlpha(double alpha)
{
	_alpha = alpha;

	if(_alpha>1)
		_alpha = 1;
	else if(_alpha < 0)
		_alpha = 0;
}
