// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 12.01.2020
// Project Name	: SmartServo
// File Name	: Servo
// Version		: 1
// Description	: This program controls the
//				  angle of the motor by
//				  checking the value of a
//				  potentiometer
//
// ------------------------------------ //

#include "Servo.h"

//----------Debug----------//
#ifdef DEBUG_MODE
double vCurrentValue;
double pidPosValue;
double pidVelValue;
double angleCurrent;
double angleCurrentFiltered;
double angleToReache;
double pidTime;
#endif
//----------Debug----------//

Servo::Servo() : pidPos(PID_POS_P,PID_POS_I,PID_POS_D), pidVel(PID_VEL_P,PID_VEL_I,PID_VEL_D), inputFilter(PID_FILTER_ALPHA)
{
	_motor.WakeUp();
	pidPos.SetMinMax(-1028, 1028);
	pidVel.SetMinMax(-100, 100);
}

void Servo::Update()
{
	if(PID_INSTANCE.Instance->CR1 & 0x0001) // Check if counter is active and running
	{
		double posError;
		double velError;
		double velocity;
		double samplePeriod = __HAL_TIM_GET_COUNTER(&PID_INSTANCE)/1000000.0;


		__HAL_TIM_SET_COUNTER(&PID_INSTANCE,0);

		_lastAngle = _angle;
		_angle = inputFilter.Calculate(adc_values[POT_ADC_INDEX]);

		velocity = _angle - _lastAngle;

		posError = pidPos.Calculate(_angleToReach, _angle, samplePeriod);

		// Prevent to accelerate to fast
		if(std::abs(posError-velocity)> _maxAcc)
			posError = velocity + std::copysign( _maxAcc, posError-velocity);

		// Calculating new PWM duty cycle with PID
		velError = pidVel.Calculate(posError, velocity, samplePeriod);

		// Set motor direction and speed depending on velError
		if(velError<-1 && adc_values[POT_ADC_INDEX]>200)
		{
			_motor.TurnRight(std::abs(velError));
			_direction = false;
		}
		else if(velError>1 && adc_values[POT_ADC_INDEX]<900)
		{
			_motor.TurnLeft(std::abs(velError));
			_direction = true;
		}
		else
			_motor.Stop(100);

		//----------Debug----------//
#ifdef DEBUG_MODE
		pidTime = 1/samplePeriod;

		angleCurrent = adc_values[POT_ADC_INDEX];
		angleCurrentFiltered = _angle;
		angleToReache = _angleToReach;

		pidPosValue = posError;
		pidVelValue = velError;

		vCurrentValue = velocity;
#endif
		//----------Debug----------//
	}
	else
	{
		// If timer is not active start it and set start angle
		HAL_TIM_Base_Start(&PID_INSTANCE);
		inputFilter.Init(adc_values[POT_ADC_INDEX]);
		_angle = adc_values[POT_ADC_INDEX];
	}
}

void Servo::Stop()
{
	_motor.Stop(40);
}
