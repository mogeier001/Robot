// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 08.02.2020
// Project Name	: SmartServo
// File Name	: MsgProcessor
// Version		: 1
// Description	: This program processes
//				  the received commands
//				  from the CAN-Bus
//
// ------------------------------------ //

#include "MsgProcessor.h"

MsgProcessor::MsgProcessor(CAN_HandleTypeDef *hcan, uint8_t id) : _id(id)
{
	this->fullMailboxesCounter = 0;
	this->_hcan = hcan;
	CAN_FilterTypeDef filter;

#if defined(SMART_SERVO_V1) || defined(SMART_SERVO_V2)
	// Create filter for moving motors on FIFO0
	filter.FilterMode = CAN_FILTERMODE_IDLIST;
	filter.FilterIdLow = FILTER_ID_MOVE_MOTOR_TO;
	filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	filter.FilterActivation = CAN_FILTER_ENABLE;
	filter.FilterScale = CAN_FILTERSCALE_16BIT;
	filter.FilterBank = FilterBank::Motor;
	HAL_CAN_ConfigFilter(this->_hcan, &filter);
#endif

#ifdef GYRO
	// Create filter for reading gyroscope on FIFO0
	filter.FilterMode = CAN_FILTERMODE_IDLIST;
	filter.FilterIdLow = FILTER_ID_READ_GYRO;
	filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	filter.FilterActivation = CAN_FILTER_ENABLE;
	filter.FilterScale = CAN_FILTERSCALE_16BIT;
	filter.FilterBank = FilterBank::Gyro;
	HAL_CAN_ConfigFilter(this->_hcan, &filter);
#endif

	// Create filter for status commands on FIFO1
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.FilterIdLow = FILTER_ID_START_UP;
	filter.FilterMaskIdLow = 0x1FF0;
	filter.FilterFIFOAssignment = CAN_FILTER_FIFO1;
	filter.FilterActivation = CAN_FILTER_ENABLE;
	filter.FilterScale = CAN_FILTERSCALE_16BIT;
	filter.FilterBank = FilterBank::Commands;
	HAL_CAN_ConfigFilter(this->_hcan, &filter);

	// Create filter for measuring sensors on FIFO1
	filter.FilterMode = CAN_FILTERMODE_IDLIST;
	filter.FilterIdLow = FILTER_ID_MEASURE;
	filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	filter.FilterActivation = CAN_FILTER_ENABLE;
	filter.FilterScale = CAN_FILTERSCALE_16BIT;
	filter.FilterBank = FilterBank::Sensors;
	HAL_CAN_ConfigFilter(this->_hcan, &filter);

	// Start CAN hardware module
	HAL_CAN_Start(this->_hcan);
}

void MsgProcessor::ProcessCommands()
{
	Message msg;
	CAN_RxHeaderTypeDef receivedMsg;
	for(int fifo = 0; fifo < 2; fifo++)
	{
		while(HAL_CAN_GetRxFifoFillLevel(this->_hcan, fifo))
		{
			HAL_CAN_GetRxMessage(this->_hcan,fifo, &receivedMsg, (uint8_t*)&msg.rawdata);
			if(msg.rawdata[0]==this->_id || msg.rawdata[0] == 0xFF)
			{
				msg.cmd = static_cast<Command>(receivedMsg.StdId);
				msg.rawdataLength = receivedMsg.DLC;
				switch (static_cast<FilterBank>(receivedMsg.FilterMatchIndex))
				{
					case FilterBank::Motor:
					{
#if defined(SMART_SERVO_V1) || defined(SMART_SERVO_V2)
						MotorMessage mMsg(&msg);
						this->msg.push(mMsg);
#endif
						break;
					}
					case FilterBank::Gyro:
					{
#ifdef GYRO
						SensorMessage gMsg(&msg);
						this->msg.push(gMsg);
#endif
						break;
					}
					case FilterBank::Commands:
						this->msg.push(msg);
						break;
					case FilterBank::Sensors:
						SensorMessage gMsg(&msg);
						this->msg.push(gMsg);
						break;
				}
			}
		}
	}
}

void MsgProcessor::SendMessages()
{
	if(HAL_CAN_GetTxMailboxesFreeLevel(this->_hcan) == 0)
	{
		fullMailboxesCounter++;
		if(fullMailboxesCounter <= 10)
		{
			for(int mailbox = 0; mailbox < 3; mailbox++)
			{
				HAL_CAN_AbortTxRequest(this->_hcan, mailbox);
			}
		}
	}
	else
	{
		fullMailboxesCounter = 0;
		while(HAL_CAN_GetTxMailboxesFreeLevel(this->_hcan) != 0 && !outgoingMsg.empty())
		{
			CAN_TxHeaderTypeDef tx = {0};

			tx.StdId = this->outgoingMsg.front().id;
			tx.IDE = CAN_ID_STD;
			tx.DLC = this->outgoingMsg.front().msg.rawdataLength;

			uint32_t availableMailbox;
			for(int mailbox = 0; mailbox < 3; mailbox++)
			{
				if(!HAL_CAN_IsTxMessagePending(this->_hcan, mailbox))
				{
					availableMailbox = mailbox;
					break;
				}
			}

			HAL_CAN_AddTxMessage(this->_hcan, &tx, (uint8_t*)&this->outgoingMsg.front().msg.rawdata, &availableMailbox);
		}
	}
}

Message MsgProcessor::PopMessage()
{
	Message msg = this->msg.front();
	this->msg.pop();
	return msg;
}

void MsgProcessor::PushMessage(Message msg, uint8_t id)
{
	MessageToSend msgTx(msg,id);
	outgoingMsg.push(msgTx);
}
