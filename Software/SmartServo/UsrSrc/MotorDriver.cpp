// ------------------------------------ //
//
// Author		: Moritz Geier
// Date			: 11.01.2020
// Project Name	: SmartServo
// File Name	: MotorDriver
// Version		: 1
// Description	: This class is a virtual
//				  interface class for the
//				  H-Bridge controllers
//
// ------------------------------------ //

#include "MotorDriver.h"

MotorDriver::MotorDriver() : period(PWM_INSTANCE.Init.Period)
{
	// Start Timer
	HAL_TIM_PWM_Start(&PWM_INSTANCE, IN2_PWM);
	HAL_TIM_PWM_Start(&PWM_INSTANCE, IN1_PWM);
}

void MotorDriver::Sleep()
{
	if(!_isAtSleep)
	{
		HAL_GPIO_WritePin(SLEEP_GPIO_Port, SLEEP_Pin, SLEEP);
		_isAtSleep = true;
	}
}

void MotorDriver::WakeUp()
{
	if(_isAtSleep)
	{
		HAL_GPIO_WritePin(SLEEP_GPIO_Port, SLEEP_Pin, SLEEP == GPIO_PIN_RESET ? GPIO_PIN_SET : GPIO_PIN_RESET);
		_isAtSleep = false;
	}
}

void MotorDriver::TurnRight(uint8_t dutyCycle)
{
	if(dutyCycle > 100)
		dutyCycle = 100;

	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN2_PWM, 0);
	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN1_PWM, (dutyCycle * period)/100.0);
}

void MotorDriver::TurnLeft(uint8_t dutyCycle)
{
	if(dutyCycle > 100)
		dutyCycle = 100;

	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN1_PWM, 0);
	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN2_PWM, (dutyCycle * period)/100.0);
}

void MotorDriver::Stop(uint8_t dutyCycle)
{
	if(dutyCycle > 100)
		dutyCycle = 100;

	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN2_PWM, (dutyCycle * period)/100.0);
	__HAL_TIM_SET_COMPARE(&PWM_INSTANCE,IN1_PWM, (dutyCycle * period)/100.0);
}
